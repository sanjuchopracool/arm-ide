#ifndef BAREEMBEDDEDPROJECTFILEEDITOR_H
#define BAREEMBEDDEDPROJECTFILEEDITOR_H
#include <texteditor/basetexteditor.h>
#include <texteditor/basetextdocument.h>
#include <coreplugin/editormanager/ieditorfactory.h>
namespace TextEditor {
class TextEditorActionHandler;
}
namespace BareEmbeddedProjectManager {
namespace Internal {

} // Internal
} //BareEmbeddedProjectManager

#endif // BAREEMBEDDEDPROJECTFILEEDITOR_H
