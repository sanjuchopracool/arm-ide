#include "bareembeddedproject.h"
#include "chopsprojectmanager.h"
#include "bareembeddedprojectmanagerconstants.h"
#include "bareembeddedprojectnode.h"
#include <projectexplorer/projectexplorer.h>
#include <coreplugin/icontext.h>
#include "CommonUtilsFunction.h"
#include <QDir>
#include <utils/fileutils.h>
#include <projectexplorer/kitinformation.h>
#include <projectexplorer/kitmanager.h>
#include <projectexplorer/target.h>
#include <qtsupport/customexecutablerunconfiguration.h>
#include <projectexplorer/toolchainmanager.h>
#include <projectexplorer/toolchain.h>
#include <projectexplorer/headerpath.h>
#include <cpptools/cppmodelmanager.h>
#include <cpptools/cppmodelmanagerinterface.h>
#include "bareembeddedprojectwizard.h"
#include <coreplugin/icore.h>
#include <QMessageBox>
#include <bareembeddedbuildconfiguration.h>
#include <QProcess>
#include <QDebug>

using namespace Core;
using namespace ProjectExplorer;
namespace BareEmbeddedProjectManager {
namespace Internal {

const QLatin1String spaceString(" ");
const QString newLineString = QLatin1String("\n");
const QString tabString = QLatin1String("\t");
#ifdef Q_OS_WIN
    //write window code here
    const QString backSlash = QLatin1String("\\");
    const QString forwardSlash = QLatin1String("/");
    const QString quoteString = QLatin1String("\"");
    const QString delCommand = QLatin1String("del ");
#else
    // for linux space is to be replaced by backslash and space
    const QString linuxSpaceEquivalent = QLatin1String("\\ ");
    const QString delCommand = QLatin1String("rm ");
#endif
/************************************************
 * Project class
 **************************************************/

struct BareEmbeddeProjectData
{
    BareEmbeddeProjectData() :
        manager(0),
        projectNode(0),
        projectDocument(0),
        headerNode(0),
        sourcesNode(0),
        linkersNode(0),
        othersNode(0)
    {}

    ChopsProjectManager* manager;
    BareEmbeddedProjectNode* projectNode;
    QString fileName;
    QString projectName;
    BareEmbeddeProjectFile* projectDocument;
    QDir projectDir;
    VirtualFolderNode* headerNode;
    VirtualFolderNode* sourcesNode;
    VirtualFolderNode* linkersNode;
    VirtualFolderNode* othersNode;
    BuildSettingParameters buildParameters;
    QStringList allFiles;
    QMap<QString, FileNode*> filesNodeMap;
    QSet<QString> includePaths;
    QStringList compilerIncludePath;
};

BareEmbeddedProject::BareEmbeddedProject(ChopsProjectManager *manager, const QString &filename)
{
    d = new BareEmbeddeProjectData;
    d->manager = manager;
    d->fileName = filename;

    //context should be personal for this plugin if don't want to see in another project
    setProjectContext(Context(BareEmbeddedProjectManager::Constants::CHOPSPROJECTCONTEXT));
    setProjectLanguages(Context(ProjectExplorer::Constants::LANG_CXX));

    QFileInfo fileInfo(d->fileName);
    d->projectDir = fileInfo.dir();

    d->projectName = fileInfo.completeBaseName();
    d->projectDocument  = new BareEmbeddeProjectFile(this, d->fileName, BareEmbeddedProject::Everything);
    d->projectNode = new BareEmbeddedProjectNode(this, d->projectDocument);
    //add all the nodes
    if(!loadProjectXmlFile())
        return;

    loadProject();
    d->manager->registerProject(this);
}

BareEmbeddedProject::~BareEmbeddedProject()
{
    d->manager->unregisterProject(this);
    delete d->projectNode;

    if(d->projectDocument)
        delete d->projectDocument;

    delete d;
    d = 0;
}

QString BareEmbeddedProject::displayName() const
{
    return d->projectName;
}

Core::Id BareEmbeddedProject::id() const
{
    return Id(Constants::CHOPSPROJECT_ID);
}

Core::IDocument *BareEmbeddedProject::document() const
{
    return d->projectDocument;
}

ProjectExplorer::IProjectManager *BareEmbeddedProject::projectManager() const
{
    return d->manager;
}

ProjectExplorer::ProjectNode *BareEmbeddedProject::rootProjectNode() const
{
    return d->projectNode;
}

QStringList BareEmbeddedProject::files(ProjectExplorer::Project::FilesMode fileMode) const
{
    Q_UNUSED(fileMode)
    return d->allFiles;
}

QStringList BareEmbeddedProject::files() const
{
    return d->allFiles;
}

bool BareEmbeddedProject::addFilesToProject(const QStringList &files)
{
    QStringList filesToAdd;
    Q_FOREACH(QString file, files)
    {
#ifdef Q_OS_WIN
        file.replace(QLatin1String("\\"), QLatin1String("/"));
#endif
        if(d->allFiles.contains(file))
            continue;

        filesToAdd.append(file);
    }

    d->allFiles.append(filesToAdd);
    addFileNodes(filesToAdd);
    emit fileListChanged();
    saveProjectFile();
    return true;
}

void BareEmbeddedProject::removeFilesFromPtoject(const QStringList &files)
{
    Q_FOREACH(QString fullFileName, files)
        d->allFiles.removeAll(fullFileName);

    removeFileNodes(files);
    emit fileListChanged();
    saveProjectFile();
}

void BareEmbeddedProject::renameFile(const QString &newName, const QString &oldName)
{
    removeFilesFromPtoject(QStringList() << oldName);
    addFilesToProject(QStringList() << newName);
    saveProjectFile();
    emit fileListChanged();
}

void BareEmbeddedProject::addIncludePath(QString path)
{
    d->includePaths.insert(path);
    addIncludePathToCppTools();
}

QStringList BareEmbeddedProject::buildTargets() const
{
    QStringList targets;
    targets.append(QLatin1String("all"));
    targets.append(QLatin1String("clean"));
    return targets;
}

QStringList BareEmbeddedProject::compilerIncludePaths() const
{
    return d->compilerIncludePath;
}

bool BareEmbeddedProject::generateMakeFile()
{
    Target* t = activeTarget();
    if(!t)
        return false;

    BuildConfiguration* configuration = t->activeBuildConfiguration();
    BareEmbeddedBuildConfiguration* bc = qobject_cast<BareEmbeddedBuildConfiguration*>(configuration);
    if(!bc)
        return false;


    QString workingDir = bc->buildDirectory();
    if(workingDir.isEmpty())
        workingDir = d->projectDir.absolutePath();

    QDir makeFileDir(workingDir);
    if(!makeFileDir.exists())
    {
        bool dirExist = makeFileDir.mkpath(workingDir);
        if(!dirExist)
        {
            //show message box here
            QMessageBox::warning(Core::ICore::mainWindow(), tr("Target folder does not exist"),
                                 tr("Failed to create makefile folder %1").arg(workingDir));
            return false;
        }
    }
    QString toolChainPrefix = bc->toolChainPrefix();

    QStringList allIncludePath = d->includePaths.toList();
    allIncludePath.append(d->compilerIncludePath);
    QString includePathString;

    BuildSettingParameters param = bc->buildSettingParameters();
    QString architectureFlag = QLatin1String(" -mcpu=$(ARM_CPU) ");
    QString cFlags = architectureFlag + param.cflags;
    QString asFlags = architectureFlag + param.asFlags;
    QString projectElf = d->projectName + QLatin1String(".elf ");
    QString projectBin = d->projectName + QLatin1String(".bin ");
    QString projectHex = d->projectName + QLatin1String(".hex ");

    //all tools string
    QString gccPath = QLatin1String("$(TOOLCHAIN)gcc");
    QString asPath = QLatin1String("$(TOOLCHAIN)as");
    QString objcopyPath = QLatin1String("$(TOOLCHAIN)objcopy");

    //for compiler path use origional string
    QString ccompilerPath = bc->toolChainPrefix() + QLatin1String("gcc");
#ifdef Q_OS_WIN
    //write window code here
    QString backSlash = QLatin1String("\\");
    QString forwardSlash = QLatin1String("/");
    QString quoteString = QLatin1String("\"");
    gccPath = quoteString + gccPath+ quoteString;
    asPath = quoteString + asPath+ quoteString;
    objcopyPath = quoteString + objcopyPath+ quoteString;
    Q_FOREACH(QString includePath, allIncludePath)
    {
        includePath.replace(backSlash, forwardSlash);
        includePathString.append(quoteString + includePath + quoteString + spaceString);
    }

    ccompilerPath = quoteString + ccompilerPath + quoteString;
#else
    // for linux space is to be replaced by backslash and space
    QString linuxSpaceEquivalent = QLatin1String("\\ ");
    toolChainPrefix.replace(spaceString, linuxSpaceEquivalent);
    Q_FOREACH(QString includePath, allIncludePath)
    {
        includePath.replace(spaceString, linuxSpaceEquivalent);
        includePathString.append(includePath + spaceString);
    }
#endif

    QString makeFileString;
    makeFileString.append(QLatin1String("TOOLCHAIN= ") + toolChainPrefix + newLineString);
    makeFileString.append(QLatin1String("ARM_CPU= ") + param.cpu + newLineString);
    makeFileString.append(QLatin1String("VPATH= ") + includePathString + newLineString);
    makeFileString.append(QLatin1String("INCLUDE= ") + includePathString + newLineString);
    makeFileString.append(QLatin1String("CCFLAGS= ") + cFlags + newLineString);
    makeFileString.append(QLatin1String("ASFLAGS= ") + asFlags + newLineString);

    QStringList AssemblerFiles;
    QStringList cFiles;
    QStringList objectList;

    Q_FOREACH(QString filePath, d->allFiles)
    {
        BareEmbeddedFileType fileType = fileTypeFromSuffix(filePath);
        if(fileType == CSOURCEFILE)
            cFiles.append(filePath);
        else if( fileType == ASSOURCEFILE)
            AssemblerFiles.append(filePath);
    }

    QStringList failedTargetList;
    QString cTargetsString =this->generateTargetForSourceFiles(cFiles, CSOURCEFILE, workingDir, includePathString,
                                                             objectList, failedTargetList,gccPath, asPath, ccompilerPath);
    QString asTargetsString = this->generateTargetForSourceFiles(AssemblerFiles, ASSOURCEFILE, workingDir, includePathString,
                                                             objectList, failedTargetList,gccPath, asPath, ccompilerPath);
    QString linkerFile = bc->linkerFilePath();
#ifdef Q_OS_WIN
    //write window code here
    linkerFile = quoteString + linkerFile + quoteString;
#else
    linkerFile.replace(spaceString,linuxSpaceEquivalent);
#endif

    //add all target
    QString allObjects = objectList.join(spaceString);
    makeFileString.append(newLineString);
    makeFileString.append(QLatin1String("all: ") + allObjects + newLineString);
    makeFileString.append(tabString + gccPath + QLatin1String(" -nostartfiles -o ")
                          + projectElf + QLatin1Literal(" -T ")
                          + linkerFile + spaceString +
                          allObjects + newLineString);
    QString removeObjectString;
    if(param.makeBinFile)
    {
        makeFileString.append(tabString + objcopyPath + QLatin1String(" -O binary ")
                              + projectElf + spaceString + projectBin + newLineString);
        removeObjectString.append(projectBin);
    }

    if(param.makeHexFile)
    {
        makeFileString.append(tabString + objcopyPath + QLatin1String(" -O ihex ")
                              + projectElf + spaceString + projectHex + newLineString);
        removeObjectString.append(projectHex);
    }

    makeFileString.append(cTargetsString);
    makeFileString.append(asTargetsString);

    //addclean target
    makeFileString.append(QLatin1String("\n\n.PHONY: clean\nclean :\n"));
    makeFileString.append(tabString + delCommand + allObjects +
                          spaceString +  projectElf + removeObjectString);

    QFile makeFile(workingDir + QLatin1String("/Makefile"));
    if(!makeFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        //show message
        QMessageBox::warning(Core::ICore::mainWindow(), tr("Failed to generate makefile"),
                             tr("Failed to generate makefile, please check the file permission for %1").
                             arg(makeFile.fileName()));
        return false;
    }

    makeFile.write(makeFileString.toAscii());

    if(failedTargetList.count())
        QMessageBox::warning(Core::ICore::mainWindow(), tr("Failed to generate targets in makefile"),
                             tr("Failed to generate makefile targets for %1").
                             arg(failedTargetList.join(spaceString)));
    return true;
}

bool BareEmbeddedProject::fromMap(const QVariantMap &map)
{
    if (!Project::fromMap(map))
        return false;


    if(!activeTarget()) //there is not active target so add a target
    {
        //find the kit
        Kit* projectKit = 0;
        QList<Kit*> kits = KitManager::instance()->kits();
        if(!kits.count())
        {
            QMessageBox::information(Core::ICore::mainWindow(), tr("No kit found"),
                                 tr("Please add an Arm kit and again open the project."));
            return false;
        }

        Q_FOREACH(Kit* kit, kits)
        {
            if(kit->id().toString() == d->buildParameters.kitId)
            {
                projectKit = kit;
                break;
            }
        }
        if(!projectKit)
        {
            KitSelectionDialog kitDialog(Core::ICore::mainWindow());
            if(kitDialog.exec() == QDialog::Accepted)
                d->buildParameters = kitDialog.buildParameters();
            else
                return false;
        }

        //again find the kit
        Q_FOREACH(Kit* kit, kits)
        {
            if(kit->id().toString() == d->buildParameters.kitId)
            {
                projectKit = kit;
                break;
            }
        }

        addTarget(createTarget(projectKit));
    }

    addIncludePathToCppTools();
    return true;
}

bool BareEmbeddedProject::setupTarget(Target *t)
{
    if(!t)
        return false;

    QStringList linkerFiles;
    Q_FOREACH(QString fileName, d->allFiles)
    {
        if(fileTypeFromSuffix(fileName) == LINKERFILE)
            linkerFiles.append(fileName);
    }

    if(!linkerFiles.count())
    {
        QMessageBox::information(Core::ICore::mainWindow(), tr("No linker file"),
                             tr("Project need at least one linker file to generate build configurations."));
        return false;
    }

    createBuildConfigurationFromLinkerFiles(t, linkerFiles);
    return true;
}

void BareEmbeddedProject::addFileNodes(const QStringList &files)
{
    Q_FOREACH(QString fullFileName, files)
    {
        QList<FileNode*> fileNodes;
        FileNode* fileNode = 0;
        BareEmbeddedFileType fileType = fileTypeFromSuffix(fullFileName);
        switch (fileType)
        {
        case CSOURCEFILE:
        case ASSOURCEFILE:
            fileNode = new FileNode(fullFileName, SourceType, false);
            fileNodes.append(fileNode);
            d->projectNode->addFileNodes(fileNodes, d->sourcesNode);
            break;
        case LINKERFILE:
            fileNode = new FileNode(fullFileName, SourceType, false);
            fileNodes.append(fileNode);
            d->projectNode->addFileNodes(fileNodes, d->linkersNode);
            this->onLinkerFileAddedd(fullFileName);
            break;
        case HEADERFILE:
            fileNode = new FileNode(fullFileName, SourceType, false);
            fileNodes.append(fileNode);
            d->projectNode->addFileNodes(fileNodes, d->headerNode);
            break;
        default:
            fileNode = new FileNode(fullFileName, UnknownFileType, false);
            fileNodes.append(fileNode);
            d->projectNode->addFileNodes(fileNodes, otherFileNode());
            break;
        }

        this->addIncludePath(QFileInfo(fullFileName).absolutePath());
        d->filesNodeMap.insert(fullFileName, fileNode);
    }
}

void BareEmbeddedProject::removeFileNodes(const QStringList &files)
{
    Q_FOREACH(QString fullFileName, files)
    {
        QList<FileNode*> fileNodes;
        FileNode* fileNode = d->filesNodeMap.value(fullFileName, 0);
        if(!fileNode)
            continue;

        fileNodes.append(fileNode);
        BareEmbeddedFileType fileType = fileTypeFromSuffix(fullFileName);
        switch (fileType)
        {
        case CSOURCEFILE:
        case ASSOURCEFILE:
            d->projectNode->removeFileNodes(fileNodes, d->sourcesNode);
            break;
        case LINKERFILE:
            d->projectNode->removeFileNodes(fileNodes, d->linkersNode);
            this->onLinkerFileRemoved(fullFileName);
            break;
        case HEADERFILE:
            d->projectNode->removeFileNodes(fileNodes, d->headerNode);
            break;
        default:
            this->removeOtherFileNode(fileNode);
            break;
        }
        d->filesNodeMap.remove(fullFileName);
    }
}

FolderNode* BareEmbeddedProject::otherFileNode()
{
    if(!d->othersNode)
    {
        d->othersNode = new VirtualFolderNode(d->projectDir.absolutePath(),-3);
        d->othersNode->setDisplayName(tr("Other"));
        d->projectNode->addFolderNodes(QList<FolderNode*>() << d->othersNode,d->projectNode);
    }

    return d->othersNode;
}

void BareEmbeddedProject::removeOtherFileNode(FileNode* fileNode)
{
    int childCount = d->othersNode->fileNodes().count();
    d->projectNode->removeFileNodes(QList<FileNode*>() << fileNode, d->othersNode);
    if(childCount == 1)
    {
        QList<FolderNode*> folderNodes;
        folderNodes.append(d->othersNode);
        d->projectNode->removeFolderNodes(folderNodes, d->projectNode);
        d->othersNode = 0;
    }
}

void BareEmbeddedProject::addIncludePathToCppTools()
{
    if(!activeTarget())
        return;

    Kit* kit = activeTarget()->kit();
    if(!kit)
        return;

    CppTools::CppModelManagerInterface *modelManager =
            CppTools::CppModelManagerInterface::instance();

    if (modelManager) {
        CppTools::CppModelManagerInterface::ProjectInfo pinfo = modelManager->projectInfo(this);
        pinfo.clearProjectParts();
        CppTools::ProjectPart::Ptr cppToolsPart(new CppTools::ProjectPart);

        if (ToolChain *tc = ToolChainKitInformation::toolChain(kit)) {
            QStringList cxxflags; // FIXME: Can we do better?
            cppToolsPart->evaluateToolchain(tc, cxxflags, cxxflags,
                                               SysRootKitInformation::sysRoot(kit));
        }

        d->compilerIncludePath = cppToolsPart->includePaths;
        cppToolsPart->includePaths += d->includePaths.toList();
        // Add any C/C++ files to be parsed
        CppTools::ProjectFileAdder adder(cppToolsPart->files);
        foreach (const QString &file, files())
            adder.maybeAdd(file);

        pinfo.appendProjectPart(cppToolsPart);
        setProjectLanguage(ProjectExplorer::Constants::LANG_CXX, true);
        modelManager->updateProjectInfo(pinfo);
    }
}

void BareEmbeddedProject::createBuildConfigurationFromLinkerFiles(Target *t,const QStringList& linkerFiles)
{
    if(!t)
        return;

    QString toolChainPrfix;
    QString makeToolPath;

    Kit* kit = t->kit();
    ToolChain *tc = ToolChainKitInformation::toolChain(kit);
    if(tc)
    {
        makeToolPath = tc->makeCommand(Utils::Environment());
        toolChainPrfix = tc->compilerCommand().toString();
        int indexOfLastHyphen = toolChainPrfix.lastIndexOf(QLatin1String("-"));
        if(indexOfLastHyphen != -1)
        toolChainPrfix = toolChainPrfix.left(indexOfLastHyphen + 1);
    }

    Q_FOREACH(QString linkerFile, linkerFiles)
    {
        //make release config
        QString linkerFileName = QFileInfo(linkerFile).baseName();
        BuildSettingParameters releaseBuildParam = d->buildParameters;
        releaseBuildParam.cflags += QLatin1String(" -O3");
        BareEmbeddedBuildConfiguration* releaseConfig = new BareEmbeddedBuildConfiguration(t);
        releaseConfig->setBuildType(BuildConfiguration::Release);
        releaseConfig->setLinkerFilePath(linkerFile);
        releaseConfig->setBuildParametes(releaseBuildParam);
        releaseConfig->setDisplayName(linkerFileName + QLatin1String("-Release"));

        //make debug config
        BuildSettingParameters debugBuildParam = d->buildParameters;
        debugBuildParam.cflags += QLatin1String(" -g");
        BareEmbeddedBuildConfiguration* debugConfig = new BareEmbeddedBuildConfiguration(t);
        debugConfig->setBuildType(BuildConfiguration::Release);
        debugConfig->setLinkerFilePath(linkerFile);
        debugConfig->setBuildParametes(debugBuildParam);
        debugConfig->setDisplayName(linkerFileName + QLatin1String("-Debug"));


        //add make steps for both
        releaseConfig->createMakeSteps();
        releaseConfig->setMakeToolPath(makeToolPath);
        releaseConfig->setToolChainPrefix(toolChainPrfix);

        debugConfig->setMakeToolPath(makeToolPath);
        debugConfig->setToolChainPrefix(toolChainPrfix);
        debugConfig->createMakeSteps();

        //add all the configurations
        t->addBuildConfiguration(releaseConfig);
        t->addBuildConfiguration(debugConfig);
    }
}

void BareEmbeddedProject::onLinkerFileRemoved(const QString &filePath)
{
    Q_FOREACH(Target* t, targets())
    {
        Q_FOREACH(BuildConfiguration* bc, t->buildConfigurations())
        {
            BareEmbeddedBuildConfiguration* bbc = qobject_cast<BareEmbeddedBuildConfiguration*>(bc);
            if(bbc && bbc->linkerFilePath() == filePath)
                t->removeBuildConfiguration(bc); //it will also delete buildConfiguration
        }
    }
}

void BareEmbeddedProject::onLinkerFileAddedd(const QString &filePath)
{
    Q_FOREACH(Target* t, targets())
        createBuildConfigurationFromLinkerFiles(t, QStringList() << filePath);
}

void BareEmbeddedProject::saveProjectFile()
{
    QByteArray fileContent = createProjectFileContent(d->buildParameters, d->allFiles);
    QFile projectFile(d->fileName);
    if(projectFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
        projectFile.write(fileContent);
}

QString BareEmbeddedProject::generateTargetForSourceFiles(const QStringList& files, BareEmbeddedFileType fileType,
                                                          const QString& workingDir, const QString& includePathString,
                                                          QStringList& objectList, QStringList& failedTargetList,
                                                          const QString& gccPath,const QString& asPath, const QString& ccompilerPath)
{
    QString makeFileString;
    Q_FOREACH(QString filePath, files)
    {
        QProcess process(this);
        process.setWorkingDirectory(workingDir);
        QStringList arguments;
        arguments.append(QLatin1String("-MM"));
        arguments.append(filePath);
        arguments.append(QLatin1String("-I") + includePathString);
        process.start(ccompilerPath, arguments);
        process.waitForFinished();
        QByteArray error = process.readAllStandardError();
        QByteArray outPut = process.readAllStandardOutput();
        if(error.size() != 0)
        {
            failedTargetList.append(filePath);
            continue;
        }

        //we have got the target for the file
        QString objectName = QFileInfo(filePath).baseName() + QLatin1String(".o");
        objectList.append(objectName);
        makeFileString.append(newLineString);
        QString outPutString = QLatin1String(outPut);

#ifdef Q_OS_WIN
        QLatin1String emptyString("");
        QStringList files = outPutString.split(QLatin1String("\n"));
        QStringList newTargetList;
        //first contain the target as well
        if(files.count())
        {
            QStringList firstLine  = files.at(0).split(QLatin1String(":"));
            QString fileName = firstLine.last();
            fileName.replace(backSlash, emptyString);
            fileName = fileName.trimmed();
            fileName = quoteString + fileName + quoteString;
            fileName = firstLine.first() + QLatin1String(":") + fileName;
            newTargetList.append(fileName);
            int filesCount = files.count();
            for(int i = 1; i < filesCount; i++)
            {
                QString fileName = files.at(i);
                if(fileName.isEmpty())
                    continue;

                fileName = fileName.trimmed();
                fileName.replace(backSlash, emptyString);
                fileName = quoteString + fileName + quoteString;
                newTargetList.append(fileName);
            }
            outPutString = newTargetList.join(spaceString + backSlash + newLineString + tabString);
        }
#else
        outPutString.replace(newLineString, newLineString + tabString);
#endif
        makeFileString.append(outPutString.trimmed());
        makeFileString.append(newLineString);

        makeFileString.append(QLatin1String("\t"));
        if(fileType == CSOURCEFILE)
        {
            makeFileString.append(gccPath);
            makeFileString.append(QLatin1String(" $(CCFLAGS) -I$(INCLUDE) -c "));
        }
        else
        {
            makeFileString.append(asPath);
            makeFileString.append(QLatin1String(" $(ASFLAGS) -o "));
            makeFileString.append(objectName);
            makeFileString.append(spaceString);
        }

#ifdef Q_OS_WIN
        filePath = quoteString + filePath + quoteString;
#else
        filePath.replace(spaceString,linuxSpaceEquivalent);
#endif
        makeFileString.append(filePath);
        makeFileString.append(newLineString);
    }
    return makeFileString;
}

void BareEmbeddedProject::loadProject()
{
    //create all the folder node
    const QString rootPath = d->projectDir.absolutePath();
    d->headerNode = new VirtualFolderNode(rootPath, 0);
    d->headerNode->setDisplayName(tr("Headers"));

    d->sourcesNode = new VirtualFolderNode(rootPath, -1);
    d->sourcesNode->setDisplayName(tr("Sources"));

    d->linkersNode = new VirtualFolderNode(rootPath,-2);
    d->linkersNode->setDisplayName(tr("Linkers"));


    //add all the folders
    QList<FolderNode*> folderNodes;
    folderNodes << d->headerNode << d->sourcesNode << d->linkersNode ;
    d->projectNode->addFolderNodes(folderNodes, d->projectNode);

    //add files
    addFileNodes(d->allFiles);
}

bool BareEmbeddedProject::loadProjectXmlFile()
{
    QFile projectFile(d->fileName);
    if(!projectFile.open(QIODevice::ReadOnly))
        return false;

    const QString projectFileContent = QLatin1String(projectFile.readAll());
    QDomDocument doc;
    if(!doc.setContent(projectFileContent))
        return false;
    QDomElement rootE = doc.documentElement();

    d->buildParameters.load(doc, rootE);

    QDomElement filesE = rootE.firstChildElement(QLatin1String("Files"));

    d->allFiles.clear();
    QDomElement fileE = filesE.firstChildElement(QLatin1String("File"));
    while(!fileE.isNull())
    {
        d->allFiles.append(fileE.text());
        fileE = fileE.nextSiblingElement(QLatin1String("File"));
    }

    return true;
}

/************************************************
 * Project File class
 **************************************************/

struct  BareEmbeddeProjectFileData
{
    BareEmbeddeProjectFileData() :
        project(0) {}

    BareEmbeddedProject *project;
    QString fileName;
    BareEmbeddedProject::RefreshOptions options;
};

BareEmbeddeProjectFile::BareEmbeddeProjectFile(BareEmbeddedProject *project, QString fileName, BareEmbeddedProject::RefreshOptions options)
{
    d = new BareEmbeddeProjectFileData;
    d->project = project;
    d->fileName = fileName;
    d->options = options;
}

bool BareEmbeddeProjectFile::save(QString *errorString, const QString &fileName, bool autoSave)
{
    Q_UNUSED(errorString)
    Q_UNUSED(fileName)
    Q_UNUSED(autoSave)
    return false;
}

QString BareEmbeddeProjectFile::fileName() const
{
    return d->fileName;
}

QString BareEmbeddeProjectFile::defaultPath() const
{
    return QString();
}

QString BareEmbeddeProjectFile::suggestedFileName() const
{
    return QString();
}

QString BareEmbeddeProjectFile::mimeType() const
{
    return QString(QLatin1String(Constants::CHOPSPROJECTMIMETYPE));
}

bool BareEmbeddeProjectFile::isModified() const
{
    return false;
}

bool BareEmbeddeProjectFile::isSaveAsAllowed() const
{
    return false;
}

void BareEmbeddeProjectFile::rename(const QString &newName)
{
    Q_UNUSED(newName);
}

Core::IDocument::ReloadBehavior BareEmbeddeProjectFile::reloadBehavior(Core::IDocument::ChangeTrigger state, Core::IDocument::ChangeType type) const
{
    Q_UNUSED(state)
    Q_UNUSED(type)
    return BehaviorSilent;
}

bool BareEmbeddeProjectFile::reload(QString *errorString, Core::IDocument::ReloadFlag flag, Core::IDocument::ChangeType type)
{
    Q_UNUSED(errorString)
    Q_UNUSED(flag)
    if (type == TypePermissions)
        return true;
    return true;
}

} // Internal
} //BareEmbeddedProjectManager
