#ifndef BAREEMBEDDEDPROJECT_H
#define BAREEMBEDDEDPROJECT_H
#include <projectexplorer/project.h>
#include <coreplugin/idocument.h>
#include <projectexplorer/projectnodes.h>
#include "CommonUtilsFunction.h"

using namespace Core;
using namespace ProjectExplorer;
namespace BareEmbeddedProjectManager {
namespace Internal {

class ChopsProjectManager;
//class BareEmbeddedProjectNode;
struct BareEmbeddeProjectData;

class BareEmbeddedProject : public ProjectExplorer::Project
{
    Q_OBJECT

public:
    BareEmbeddedProject(ChopsProjectManager *manager, const QString &filename);
    ~BareEmbeddedProject();

    //Project class Implementation
    QString displayName() const;
    Core::Id id() const;
    Core::IDocument *document() const;
    ProjectExplorer::IProjectManager *projectManager() const;
    ProjectExplorer::ProjectNode *rootProjectNode() const;

    QStringList files(FilesMode fileMode) const;
    QStringList files() const;

    enum RefreshOptions {
        Files         = 0x01,
        Configuration = 0x02,
        Everything    = Files | Configuration
    };

    void refresh(RefreshOptions options);
    bool addFilesToProject(const QStringList& files);
    void removeFilesFromPtoject(const QStringList& files);
    void renameFile(const QString& newName, const QString& oldName);
    void addIncludePath(QString path);

    QStringList buildTargets() const;
    QStringList compilerIncludePaths() const;

    bool generateMakeFile();

protected:
    bool fromMap(const QVariantMap &map);
    bool setupTarget(Target *t);

private:
    BareEmbeddeProjectData* d;
    void loadProject();
    bool loadProjectXmlFile();
    void addFileNodes(const QStringList& files);
    void removeFileNodes(const QStringList& files);
    FolderNode *otherFileNode();
    void removeOtherFileNode(ProjectExplorer::FileNode *fileNode);
    void addIncludePathToCppTools();
    void createBuildConfigurationFromLinkerFiles(Target* t, const QStringList &linkerFiles);
    void onLinkerFileRemoved(const QString& filePath);
    void onLinkerFileAddedd(const QString& filePath);
    void saveProjectFile();
    QString generateTargetForSourceFiles(const QStringList& files, BareEmbeddedFileType fileType,
                                         const QString& workingDir, const QString& includePathString,
                                         QStringList& objectList, QStringList& failedTargetList,
                                         const QString &gccPath,const QString& asPath,
                                         const QString &ccompilerPath);
};


struct BareEmbeddeProjectFileData;
class BareEmbeddeProjectFile : public Core::IDocument
{
    Q_OBJECT

public:
    BareEmbeddeProjectFile(BareEmbeddedProject *project, QString fileName, BareEmbeddedProject::RefreshOptions options);

    bool save(QString *errorString, const QString &fileName, bool autoSave);
    QString fileName() const;

    QString defaultPath() const;
    QString suggestedFileName() const;
    QString mimeType() const;

    bool isModified() const;
    bool isSaveAsAllowed() const;
    void rename(const QString &newName);

    ReloadBehavior reloadBehavior(ChangeTrigger state, ChangeType type) const;
    bool reload(QString *errorString, ReloadFlag flag, ChangeType type);

private:
    BareEmbeddeProjectFileData* d;
};

} // Internal
} //BareEmbeddedProjectManager

#endif // BAREEMBEDDEPROJECT_H
