#include "bareembeddedprojectnode.h"
#include "bareembeddedproject.h"

namespace BareEmbeddedProjectManager {
namespace Internal {

struct BareEmbeddedProjectNodeData
{
    BareEmbeddedProjectNodeData() :
        project(0),
        document(0)
    {}
    BareEmbeddedProject* project;
    Core::IDocument* document;
};

BareEmbeddedProjectNode::BareEmbeddedProjectNode(BareEmbeddedProject *project, Core::IDocument *projectFile) :
    ProjectNode(projectFile->fileName())
{
    d = new BareEmbeddedProjectNodeData;
    d->project = project;
    d->document = projectFile;
    setDisplayName(QFileInfo(projectFile->fileName()).completeBaseName());
}

BareEmbeddedProjectNode::~BareEmbeddedProjectNode()
{
    delete d;
    d = 0;
}

Core::IDocument *BareEmbeddedProjectNode::projectFile() const
{
    return d->document;
}

QString BareEmbeddedProjectNode::projectFilePath() const
{
    return d->document->fileName();
}

bool BareEmbeddedProjectNode::hasBuildTargets() const
{
    return true;
}

QList<ProjectExplorer::ProjectNode::ProjectAction> BareEmbeddedProjectNode::supportedActions(ProjectExplorer::Node *node) const
{
    Q_UNUSED(node);
    return QList<ProjectAction>()
            << AddNewFile
            << AddExistingFile
            << RemoveFile
            << Rename;
}

bool BareEmbeddedProjectNode::canAddSubProject(const QString &proFilePath) const
{
    Q_UNUSED(proFilePath)
    return false;
}

bool BareEmbeddedProjectNode::addSubProjects(const QStringList &proFilePaths)
{
    Q_UNUSED(proFilePaths)
    return false;
}

bool BareEmbeddedProjectNode::removeSubProjects(const QStringList &proFilePaths)
{
    Q_UNUSED(proFilePaths)
    return false;
}

bool BareEmbeddedProjectNode::addFiles(const FileType fileType, const QStringList &filePaths, QStringList *notAdded)
{
    Q_UNUSED(fileType)
    Q_UNUSED(notAdded)
    return d->project->addFilesToProject(filePaths);
}

bool BareEmbeddedProjectNode::removeFiles(const FileType fileType, const QStringList &filePaths, QStringList *notRemoved)
{
    Q_UNUSED(fileType)
    Q_UNUSED(notRemoved)
    d->project->removeFilesFromPtoject(filePaths);
    return true;
}

bool BareEmbeddedProjectNode::deleteFiles(const FileType fileType, const QStringList &filePaths)
{
    Q_UNUSED(fileType)
    d->project->removeFilesFromPtoject(filePaths);
    return true;
}

bool BareEmbeddedProjectNode::renameFile(const FileType fileType, const QString &filePath, const QString &newFilePath)
{
    Q_UNUSED(fileType)
    d->project->renameFile(newFilePath, filePath);
    return true;
}

QList<ProjectExplorer::RunConfiguration *> BareEmbeddedProjectNode::runConfigurationsFor(ProjectExplorer::Node *node)
{
    Q_UNUSED(node)
    return QList<ProjectExplorer::RunConfiguration *>();
}

} // Internal
} //BareEmbeddedProjectManager
