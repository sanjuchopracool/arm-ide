#include "AddNewFile.h"
#include "ui_AddNewFile.h"
#include <QStandardItem>
#include <QDebug>
#include "Constants.h"
#include "CreateFileDialog.h"

AddNewFile::AddNewFile(QWidget *parent, Project *project) :
    QDialog(parent),
    ui(new Ui::AddNewFile)
{
    ui->setupUi(this);
    m_project = project;
    m_model = new QStandardItemModel(this);
    setWindowTitle(tr("New file wizard"));
    QStandardItem* item ;
    item = new QStandardItem("Header file");
    item->setEditable(false);
    m_model->setItem(0, item);
    item = new QStandardItem("C source file");
    item->setEditable(false);
    m_model->setItem(1, item);
    item = new QStandardItem("Assembly file");
    item->setEditable(false);
    m_model->setItem(2, item);
    item = new QStandardItem("Other file");
    item->setEditable(false);
    m_model->setItem(3, item);

    ui->lvFileType->setModel(m_model);

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(slotOkClicked()));
}

AddNewFile::~AddNewFile()
{
    delete m_model;
    delete ui;
}

void AddNewFile::slotOkClicked()
{
    int index = ui->lvFileType->currentIndex().row();
    FileType type;
    switch(index) {
    case 0:
        type = HEADERFILE;
        break;
    case 1:
        type = CFILE;
        break;
    case 2:
        type = ASSSEMBLYFILE;
        break;
    case 3:
        type = OTHERFILE;
        break;
    default:
        return;
    }

    CreateFileDialog createFileDialog(this, m_project,type);
    createFileDialog.exec();
    this->accept();
}
