#include "ProjectFile.h"
#include "ProjectData.h"
#include "SoftwareDefaults.h"
#include "QDir"
struct ProjectFileData
{
    ProjectFileData() : project(0), modified(false) {}
    Project* project;
    QString fileName;
    FileType type;
    QString fullPath;
    bool modified;
};

ProjectFile::ProjectFile(Project *project, const QString &name)
{
    d= new ProjectFileData;
    d->project = project;
    d->fileName = name;
    d->type = SoftwareDefaults::instance().fileTypeFromSuffix(name);
    switch (d->type) {
    case CFILE:
    case ASSSEMBLYFILE:
        d->fullPath = project->projectPath() + slash + "src" + slash + name;
        break;
    case LINKERFILE:
        d->fullPath = project->projectPath() + slash + "other" + slash + name;
        break;
    case HEADERFILE:
        d->fullPath = project->projectPath() + slash + "include" + slash + name;
        break;
    default:
        d->fullPath = project->projectPath() + slash + "other" + slash + name;
        break;
    }

}

Project *ProjectFile::project() const
{
    return d->project;
}

void ProjectFile::changeName(const QString& newName)
{
    QString fileDirectory = d->fullPath;
    QString fileName = d->fileName;
    fileDirectory = fileDirectory.remove(fileDirectory.length() - fileName.length() - 1 ,fileName.length() + 2);
    if(QDir(fileDirectory).rename(d->fileName,newName))
    {
        d->fullPath = fileDirectory + slash + newName;
        d->fileName = newName;
    }
}

QString ProjectFile::name()
{
    return d->fileName;
}

FileType ProjectFile::fileType() const
{
    return d->type;
}

QString ProjectFile::filePath()
{
    return d->fullPath;
}

bool ProjectFile::modified() const
{
    return d->modified;
}

void ProjectFile::setModified(bool modify)
{
    d->modified = modify;
}
