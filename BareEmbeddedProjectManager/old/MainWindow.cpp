#include "MainWindow.h"
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QDockWidget>
#include <QStyle>
#include <QDebug>
#include <QFile>
#include "FirstTimeSetupDialog.h"
#include <QDomDocument>
#include "LinkerConfigDialog.h"
#include "StartUp.h"
#include <QFileDialog>
#include "SoftwareDefaults.h"
#include "NewProject.h"
#include "ProjectSettingDialog.h"
#include <QTreeWidget>
#include "ProjectData.h"
#include "ProjectFile.h"
#include "AddNewFile.h"
#include <QMessageBox>
#include <QInputDialog>
#include <QPlainTextEdit>
#include <QProcess>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    m_currentProject = 0;
    m_lastProject = 0;
    m_makeTool = new MakeTool(this);

    setWindowTitle("Arm-Ide");
    //Read settings
    SoftwareDefaults::instance().load();

    //check whether configuration for toolchain exist and is valid
    checkExternalTools();

    //add docks at present only the projectexplorer
    createDocks();

    // create action, menu , toolbars and other things
    createActions();
    createMenus();
    enableProjectMenu(false);


    // create centralwidget
    m_workSpace = new CentralTabWidget(this);
    setCentralWidget(m_workSpace);

    showMaximized();
    setMinimumSize(600,400);
}

MainWindow::~MainWindow()
{
    
}

Project *MainWindow::currentProject()
{
    return m_currentProject;
}

MainWindow& MainWindow::instance()
{
    static MainWindow theInstance;
    return theInstance;
}

ProjectModel* MainWindow::projectModel()
{
    return m_model;
}

ProjectExplorer *MainWindow::projectTree()
{
    return m_explorer;
}

void MainWindow::newProject()
{
    m_lastProject = m_currentProject;
    m_currentProject = new Project(this);

    NewProject projectDialog(this,m_currentProject);
    projectDialog.exec();
    if(projectDialog.returnCode() == 0)
    {
        m_projects.append(m_currentProject);
        m_model->addProject(m_currentProject);
        enableProjectMenu(true);
        // open main.c File
        m_workSpace->openFile(m_currentProject->file("main.c"));
    }
    else
    {
        delete m_currentProject;
        m_currentProject = m_lastProject ;
    }
}

void MainWindow::openProject()
{
    QString projectFileName = QFileDialog::getOpenFileName(this, tr("Choose existing project"),
                                                           SoftwareDefaults::instance().defaultProjectDir(),
                                                           tr("Arm Project (*.chops)"));

    if(projectFileName.isEmpty())
        return;

    QDomDocument projectXml("chops");
    QFile projectFile(projectFileName);
    if(!projectFile.open(QIODevice::ReadOnly))
        qDebug() << "Failed to open project file in read only mode";

    QString xmlString = projectFile.readAll();

    projectFile.close();

    if(xmlString.isEmpty())
        return;

    if(!projectXml.setContent(xmlString))
        qDebug() << "Unable to parse xml file";

    m_lastProject = m_currentProject;
    m_currentProject = new Project(this);

    QDomElement projectE = projectXml.firstChildElement("project");
    m_currentProject->load(projectXml, projectE);
    m_projects.append(m_currentProject);
    m_model->addProject(m_currentProject);
    enableProjectMenu(true);

}

void MainWindow::closeProject()
{
    if(!m_currentProject)
        return;
    closeProject(m_currentProject, false, true);
}

void MainWindow::closeProject(Project *project, bool yesAll, bool close)
{
    if(!project)
        return;

    if(project == m_currentProject)
    {
        if(m_projects.contains(m_lastProject))
        {
            m_currentProject = m_lastProject;

            if(m_currentProject == project)
            {
                if(m_projects.count() == 1)
                {
                    enableProjectMenu(false);
                    m_lastProject = m_currentProject = 0;
                }
                else
                    Q_FOREACH(Project* proj, m_projects)
                    {
                        if(proj != project)
                        {
                            m_currentProject = proj;
                            m_model->setActiveProject(proj);
                            break;
                        }
                    }
            }
            else
                m_model->setActiveProject(m_currentProject);
        }
        else
        {
            enableProjectMenu(false);
            m_lastProject = m_currentProject = 0;
        }
    }

    bool yesToAll = yesAll;
    Q_FOREACH(ProjectFile* file, project->files())
    {
        if(file->modified())
        {
            if(!yesToAll)
            {
                int answer = QMessageBox::warning(this,tr("Project is modified"),
                                                  tr("%1 file have been changed. Do you want to save?").arg(file->name()),
                                                  QMessageBox::Yes,QMessageBox::YesToAll,QMessageBox::No);
                switch(answer) {
                case QMessageBox::Yes:
                    m_workSpace->saveFileAndClose(file, close);
                    break;
                case QMessageBox::YesToAll:
                    m_workSpace->saveFileAndClose(file, close);
                    yesToAll = true;
                    break;
                default:
                    break;
                }
            }
            else
                m_workSpace->saveFileAndClose(file, close);
        }
        m_workSpace->closeFile(file);
    }

    m_model->removeProject(project);
    m_projects.removeOne(project);
    delete project;
}

void MainWindow::slotSetActiveProject(Project *project)
{
    if(!project)
        return;

    if(m_projects.contains(project))
    {
        m_lastProject =m_currentProject;
        m_currentProject = project;
        m_model->setActiveProject(project);
    }
}

void MainWindow::checkExternalTools()
{
    if(!SoftwareDefaults::instance().defaultSettingPresent())
    {
        FirstTimeSetupDialog toolChainSelectDialog(this);
        toolChainSelectDialog.exec();
    }
}

void MainWindow::createActions()
{

    m_newProjectAction = new QAction(style()->standardIcon(QStyle::SP_FileIcon),
                                     QString(tr("&New project")),this);
    m_newProjectAction->setShortcuts(QKeySequence::New);
    m_newProjectAction->setStatusTip("Create a new Project");
    connect(m_newProjectAction,SIGNAL(triggered()),this,SLOT(newProject()));

    m_openProjectAction = new QAction(style()->standardIcon(QStyle::SP_DialogOpenButton),
                                      QString(tr("&Open project")),this);
    m_openProjectAction->setShortcuts(QKeySequence::Open);
    connect(m_openProjectAction, SIGNAL(triggered()), this, SLOT(openProject()));


    m_closeCurrentProjectAction = new QAction(style()->standardIcon(QStyle::SP_DialogCloseButton),
                                              QString(tr("&Close Active project")),this);
    //m_closeCurrentProjectAction->setShortcuts(QKeySequence::Open);
    connect(m_closeCurrentProjectAction, SIGNAL(triggered()), this, SLOT(closeProject()));
    m_closeCurrentProjectAction->setEnabled(false);

    //quit Action
    m_quitAction = new QAction(tr("E&xit"),this);
    m_quitAction->setShortcuts(QKeySequence::Quit);
    connect(m_quitAction,SIGNAL(triggered()),qApp,SLOT(quit()));



    //project Actions
    m_linkerSettingAction = new QAction(tr("&Linker Setting"),this);
    connect(m_linkerSettingAction,SIGNAL(triggered()),this,SLOT(changeLinkerSetting()));
    //setting actions
    m_toolChainAction = new QAction(tr("Change &toolchain"),this);
    connect(m_toolChainAction,SIGNAL(triggered()),this,SLOT(changeToolChain()));

    m_startUpCodeAction = new QAction(tr("StartUp Co&de"),this);
    connect(m_startUpCodeAction,SIGNAL(triggered()),this,SLOT(changeStartUpCode()));

    m_compilerSetting = new QAction(tr("&Compiler setting"),this);
    connect(m_compilerSetting,SIGNAL(triggered()),this,SLOT(changeCompilerSetting()));


}

void MainWindow::createMenus()
{
    m_fileMenu = menuBar()->addMenu("&File");
    m_fileMenu->addAction(m_newProjectAction);
    m_fileMenu->addAction(m_openProjectAction);
    m_fileMenu->addSeparator();
    m_fileMenu->addAction(m_closeCurrentProjectAction);
    m_fileMenu->addSeparator();
    m_fileMenu->addAction(m_quitAction);


    m_projectMenu = menuBar()->addMenu("&Project");
    m_projectMenu->addAction(m_linkerSettingAction);
    m_projectMenu->addAction(m_startUpCodeAction);
    m_projectMenu->addAction(m_compilerSetting);

    m_settingMenu = menuBar()->addMenu("&Setting");
    m_settingMenu->addAction(m_toolChainAction);

    m_dockMenu =  menuBar()->addMenu("Doc&k");
    QAction* dockAction = m_projectExplorerDock->toggleViewAction();
    if(dockAction)
    {
        dockAction->setText("Project explorer");
        m_dockMenu->addAction(dockAction);
    }
    dockAction = m_logDock->toggleViewAction();
    if(dockAction)
    {
        dockAction->setText("Build log");
        m_dockMenu->addAction(dockAction);
    }
}

void MainWindow::createDocks()
{
//    setCorner( Qt::TopLeftCorner, Qt::LeftDockWidgetArea );
//    setCorner( Qt::TopRightCorner, Qt::RightDockWidgetArea );
//    setCorner( Qt::BottomLeftCorner, Qt::LeftDockWidgetArea );
//    setCorner( Qt::BottomRightCorner, Qt::RightDockWidgetArea );
    setDockOptions(QMainWindow::ForceTabbedDocks | QMainWindow::VerticalTabs);

    m_projectExplorerDock = new QDockWidget(tr("Projects"),this);
    m_projectExplorerDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    m_model = new ProjectModel(this);
    m_explorer = new ProjectExplorer(this);
    m_explorer->setModel(m_model);
    m_projectExplorerDock->setWidget(m_explorer);

//    QDockWidget* dock2 = new QDockWidget("Dock 2" , this);
//    dock2->setWidget(new QWidget());
    addDockWidget(Qt::LeftDockWidgetArea,m_projectExplorerDock);
    //    tabifyDockWidget(m_projectExplorerDock,dock2);

    connect(m_explorer, SIGNAL(openFile(Project*,QString)), this, SLOT(slotOpenFile(Project*,QString)));
    connect(m_explorer, SIGNAL(closeProject(Project*)), this, SLOT(closeProject(Project*)));
    connect(m_explorer, SIGNAL(setActiveProject(Project*)), this,SLOT(slotSetActiveProject(Project*)));
    connect(m_explorer, SIGNAL(addNewFile(Project*)),this, SLOT(slotAddNewFile(Project*)));
    connect(m_explorer, SIGNAL(removeFile(Project*,QString)), this, SLOT(slotRemoveFile(Project*,QString)));
    connect(m_explorer, SIGNAL(renameFile(Project*,QString)), this, SLOT(slotRenameFile(Project*,QString)));
    connect(m_explorer, SIGNAL(generateMakeFile(Project*)), this , SLOT(slotGenerateMakeFile(Project*)));
    connect(m_explorer, SIGNAL(cleanProject(Project*)), this , SLOT(slotCleanProject(Project*)));
    connect(m_explorer, SIGNAL(buildProject(Project*)), this , SLOT(slotBuildProject(Project*)));
    connect(m_explorer, SIGNAL(rebuildProject(Project*)), this , SLOT(slotRebuildProject(Project*)));

    m_logDock = new QDockWidget(tr("Build log"), this);
    m_logDock->setAllowedAreas(Qt::BottomDockWidgetArea);
    m_logEditor = new QPlainTextEdit(this);
    m_logEditor->setReadOnly(true);
   // m_logEditor->setBackgroundRole(QPalette::Dark);
    m_logDock->setWidget(m_logEditor);
    addDockWidget(Qt::BottomDockWidgetArea, m_logDock);
}


void MainWindow::enableProjectMenu(bool flag)
{
    m_projectMenu->setEnabled(flag);
    m_closeCurrentProjectAction->setEnabled(flag);
}

void MainWindow::changeToolChain()
{
    FirstTimeSetupDialog toolchainDialog(this);
    toolchainDialog.exec();
}

void MainWindow::changeLinkerSetting()
{
    if(!m_currentProject)
        return;
    LinkerConfigDialog linkerDialog(this, m_currentProject);
    linkerDialog.exec();
}

void MainWindow::changeStartUpCode()
{
    if(!m_currentProject)
        return;
    StartUp startUpCodeDialog(this, m_currentProject);
    startUpCodeDialog.exec();
}

void MainWindow::changeCompilerSetting()
{
    if(!m_currentProject)
        return;
    ProjectSettingDialog compilerSettingDialog(this, m_currentProject);
    compilerSettingDialog.exec();
}

void MainWindow::slotAddNewFile(Project *project)
{
    if(!project)
        return;
    AddNewFile newFileDialog(this, project);
    newFileDialog.exec();
}

void MainWindow::slotRemoveFile(Project *project, const QString& fileName)
{
    if(!project)
        return;
    ProjectFile* file = project->file(fileName);
    if(!file)
        return;

    m_workSpace->closeFile(file);
    m_model->removeFileFromProject(project, fileName);
    deleteFileFromDisk(file, true);
    project->removeFile(fileName);
    project->updateProjectFile();

}

void MainWindow::slotOpenFile(Project *project, const QString& fileName)
{
    if(!project)
        return;
    ProjectFile* file = project->file(fileName);
    if(!file)
        return;
    m_workSpace->openFile(file);
}

void MainWindow::slotRenameFile(Project* project, const QString & fileName)
{
    if(!project)
        return;

    bool ok;
    QString newName = QInputDialog::getText(this, tr("Enter new name"),
                                            tr("Enter new name for file.\nMake sure file extension is same!!!"),
                                            QLineEdit::Normal,fileName, &ok);
    if(ok && !newName.isEmpty())
    {
        project->renameFile(fileName, newName);
        m_model->renameFileFromProject(project, fileName, newName);
        project->updateProjectFile();
    }
}

void MainWindow::slotGenerateMakeFile(Project *project)
{
    if(!project)
        return;
    m_makeTool->generateMakeFileForProject(project);
}

void MainWindow::slotCleanProject(Project *project)
{
    if(!project)
        return;

    if(!QFile(project->projectPath() + slash + "Makefile").exists())
        slotGenerateMakeFile(project);

    QProcess* process = new QProcess;
    process->start("./clean", QStringList() << project->projectPath());
    process->waitForFinished();
    QString output = process->readAllStandardOutput();
    QString error = process->readAllStandardError();
    delete process;
    m_logEditor->clear();
    m_logEditor->appendPlainText(output);
    m_logEditor->appendPlainText(error);
}

void MainWindow::slotBuildProject(Project *project)
{
    if(!project)
        return;

    if(!QFile(project->projectPath() + slash + "Makefile").exists())
        slotGenerateMakeFile(project);

    QProcess* process = new QProcess;
    process->start("./build", QStringList() << project->projectPath());
    process->waitForFinished();
    QString output = process->readAllStandardOutput();
    QString error = process->readAllStandardError();
    delete process;
    m_logEditor->clear();
    m_logEditor->appendPlainText(output);
    m_logEditor->appendPlainText(error);
}

void MainWindow::slotRebuildProject(Project *project)
{
    slotCleanProject(project);
    slotBuildProject(project);
}

void MainWindow::deleteFileFromDisk(ProjectFile *file, bool showMessage)
{
    QString fileDirectory = file->filePath();
    QString fileName = file->name();
    fileDirectory = fileDirectory.remove(fileDirectory.length() - fileName.length() - 1 ,fileName.length() + 2);
    if(showMessage)
    {
        int answer = QMessageBox::warning(this,tr("Delete file"),
                                          tr("Are you sure youwant to delete file %1 ?").arg(fileName),
                                          QMessageBox::No,QMessageBox::Yes);
        if(answer == QMessageBox::Yes)
            QDir(fileDirectory).remove(fileName);
        else
            return;
    }
    else
        QDir(fileDirectory).remove(fileName);
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    Q_UNUSED(event);
    bool yesToAll = false;
    Q_FOREACH(Project* project, m_projects)
    {
        if(project->modified())
        {
            if(!yesToAll)
            {
                int answer = QMessageBox::warning(this, tr("Project modified"),
                                                  tr("%1 project is modified, do you want to save?").arg(project->projectName()),
                                                  QMessageBox::Yes,QMessageBox::YesToAll,QMessageBox::No);
                switch(answer) {
                case QMessageBox::Yes:
                    closeProject(project, true, true);
                    break;
                case QMessageBox::YesToAll:
                    closeProject(project, true, true);
                    yesToAll = true;
                    break;
                default:
                    break;
                }
            }
            else
                closeProject(project, true, true);
        }
    }
}
