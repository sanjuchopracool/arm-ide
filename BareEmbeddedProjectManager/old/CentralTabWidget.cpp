#include "CentralTabWidget.h"
#include "FindReplaceWidget.h"
#include <QFile>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QDebug>

struct CentralTabWidgetData
{
    CentralTabWidgetData() {}
    FindReplaceWidget* findWidget;
    QTabWidget* tabWidget;
    QVBoxLayout* layout;
    QMap<ProjectFile*,QWidget*> fileEditorMap;

};

CentralTabWidget::CentralTabWidget(QWidget* parent) :
    QWidget(parent)
{
    d = new CentralTabWidgetData;
    d->tabWidget = new QTabWidget(this);
    d->tabWidget->setMovable(true);
    d->findWidget = new FindReplaceWidget();
    d->layout = new QVBoxLayout(this);

    //add in layout
    d->layout->addWidget(d->tabWidget);
    d->layout->addWidget(d->findWidget);
    //initialy hide th find replace widget
    d->findWidget->hide();

    d->tabWidget->setTabsClosable(true);


    //connect all signals
    connect(d->tabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(slotTabCloseRequested(int)));
}

CentralTabWidget::~CentralTabWidget()
{
    delete d;
    d = 0;
}

void CentralTabWidget::openFile(ProjectFile* file)
{
    if(!file)
        return;

    if(d->fileEditorMap.contains(file))
    {
        setCurrentEditor(d->fileEditorMap.value(file));
        return;
    }
    AbstractEditor* editor = new AbstractEditor(this);
    d->tabWidget->addTab(editor,file->name());
    d->tabWidget->setCurrentIndex(d->tabWidget->count() -1);
    d->tabWidget->setTabToolTip(d->tabWidget->currentIndex(), file->filePath());
    d->fileEditorMap.insert(file, editor);
    editor->setFocus();
    QFile openFile(file->filePath());
    if(openFile.open(QIODevice::ReadOnly))
    {
        editor->setText(openFile.readAll());
        openFile.close();
    }
    connect(editor, SIGNAL(findWidgetRequested(QString, bool)), this, SLOT(slotFindWidgetReuested(QString, bool)));
    connect(editor, SIGNAL(hideFindWidgetRequest()), this, SLOT(slotHide()));
    connect(editor, SIGNAL(textChanged()), this, SLOT(slotTextModified()));
    connect(editor, SIGNAL(saveRequested(AbstractEditor*)), this, SLOT(slotSaveRequested(AbstractEditor*)));
}

void CentralTabWidget::setCurrentEditor(QWidget* widget)
{
    if(!widget)
        return;

    int count = d->tabWidget->count();
    for( int i = 0 ; i < count; i++)
    {
        if(d->tabWidget->widget(i) == widget)
            d->tabWidget->setCurrentIndex(i);
    }
}

void CentralTabWidget::saveFile(ProjectFile* file, AbstractEditor* editor)
{
    if(!file || !editor)
        return;
    QFile saveFile(file->filePath());
    if(!saveFile.open(QIODevice::WriteOnly))
    {
        QMessageBox::warning(this, tr("Error opening file!"), tr("Failed to open %1 file in write mode.").arg(file->name()));
        return;
    }
    QTextStream stream(&saveFile);
    stream << editor->text();
    saveFile.close();
    stream.flush();
    file->setModified(false);
    int index = d->tabWidget->indexOf(editor);
    if(index == -1)
        return;
    d->tabWidget->setTabText(index, file->name());
}

void CentralTabWidget::saveFileAndClose(ProjectFile* file, bool close)
{
    if(file)
        return;

    //not checking the pointer because other things are
    QWidget* widget = d->fileEditorMap.value(file,0);
    int index = d->tabWidget->indexOf(widget);
    if(index == -1)
        return;

    AbstractEditor* editor = static_cast<AbstractEditor*>(widget);
    if(!editor)
        return;

    saveFile(file, editor);
    if(close)
    {
        disconnect(editor, 0, 0, 0);
        d->tabWidget->removeTab(index);
        d->fileEditorMap.remove(file);
        delete editor;
    }
}

void CentralTabWidget::closeFile(ProjectFile *file)
{
    if(!file)
        return;

    //not checking the pointer because other things are
    QWidget* widget = d->fileEditorMap.value(file,0);
    int index = d->tabWidget->indexOf(widget);
    if(index == -1)
        return;

    disconnect(widget, 0, 0, 0);
    d->tabWidget->removeTab(index);
    d->fileEditorMap.remove(file);
    delete widget;
}

void CentralTabWidget::slotTabCloseRequested(int index)
{
    AbstractEditor* editor = static_cast<AbstractEditor*>(d->tabWidget->widget(index));
    if(!editor)
        return;

    ProjectFile* file = d->fileEditorMap.key(editor,0);
    if(!file)
        return;

    if(file->modified())
    {
        int button =  QMessageBox::warning(this, tr("Save file"),tr("Text is modified. Do you want to save the file ?"),
                                           QMessageBox::Yes,QMessageBox::No,QMessageBox::Cancel);
        switch(button) {
        case QMessageBox::Yes:
            saveFile(file, editor);
        case QMessageBox::No:
            file->setModified(false);
            break;
        case QMessageBox::Cancel:
            return;
            break;
        default:
            break;
        }
    }

    disconnect(editor, 0, 0, 0);
    d->tabWidget->removeTab(index);
    d->fileEditorMap.remove(file);
    delete editor;
}

void CentralTabWidget::slotFindWidgetReuested(const QString &searchFor, bool replaceSupport)
{
    d->findWidget->showReplaceControl(replaceSupport);
    d->findWidget->setSearchText(searchFor);
    d->findWidget->show();
}

void CentralTabWidget::slotHide()
{
    d->findWidget->hide();
}

void CentralTabWidget::slotTextModified()
{
   ProjectFile* file = d->fileEditorMap.key(d->tabWidget->currentWidget(),0);
   if(!file)
       return;

   file->setModified(true);
   d->tabWidget->setTabText(d->tabWidget->currentIndex(), file->name() + "*");
}

void CentralTabWidget::slotSaveRequested(AbstractEditor *editor)
{
    if(!editor)
        return;
    ProjectFile* file = d->fileEditorMap.key(editor,0);
    saveFile(file, editor);
}
