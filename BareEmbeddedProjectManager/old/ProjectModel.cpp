#include "ProjectModel.h"
#include <QMap>
#include "MainWindow.h"
#include <QDebug>
#include "SoftwareDefaults.h"

struct ProjectModelData
{
    QList<Project*> projects;
    QMap <Project*,QStandardItem*> projectsItemMap;
};

ProjectModel::ProjectModel(QObject *parent) :
    QStandardItemModel(parent)
{
    d = new ProjectModelData;
}

//ProjectModel &ProjectModel::instance()
//{
//    static ProjectModel theInstance;
//    return theInstance;
//}

void ProjectModel::addProject(Project* project)
{
    if(!project)
        return;

    if(d->projects.contains(project))
        return;

    d->projects.append(project);
    QStandardItem* projectItem = modelItemForProject(project);
    if(projectItem)
    {
        d->projectsItemMap.insert(project,projectItem);
        appendRow(projectItem);
        setActiveProject(project);
        MainWindow* mainWindow = static_cast<MainWindow*>(project->parent());
        if(mainWindow)
        {
            mainWindow->projectTree()->expand(projectItem->index());
            mainWindow->projectTree()->expand(projectItem->child(0)->index());
            mainWindow->projectTree()->expand(projectItem->child(1)->index());
            mainWindow->projectTree()->expand(projectItem->child(2)->index());
        }
    }

}

void ProjectModel::removeProject(Project* project)
{
    if(!project)
        return;

    if(!d->projects.contains(project))
        return;

    d->projects.removeAt(d->projects.indexOf(project));
    QStandardItem* projectItem = d->projectsItemMap.value(project);
    removeRow(projectItem->row(),projectItem->index().parent());
    //delete projectItem;
    d->projectsItemMap.remove(project);

}

QStandardItem* ProjectModel::modelItemForProject(Project *project)
{
    QStandardItem* projectItem = 0;
    if(!project)
        return projectItem;

    projectItem = new QStandardItem(QIcon(":/images/project.png"),project->projectName() + ".chops");
    projectItem->setData(Project::projectToVaraint(project), Qt::UserRole);
    QStandardItem* headers = new QStandardItem(QIcon(":/images/folder.png"),"Headers");
    headers->appendRows(modelItemsForStringList(project->headers()));
    QStandardItem* sources = new QStandardItem(QIcon(":/images/folder.png"),"Sources");
    sources->appendRows(modelItemsForStringList(project->sources()));
    QStandardItem* others = new QStandardItem(QIcon(":/images/folder.png"),"Others");
    others->appendRows(modelItemsForStringList(project->others()));

    projectItem->appendRow(headers);
    projectItem->appendRow(sources);
    projectItem->appendRow(others);

    return projectItem;
}

QList<QStandardItem *> ProjectModel::modelItemsForStringList(const QStringList& list)
{
    QList<QStandardItem*> itemList;
    Q_FOREACH(QString item,list)
    {
        QStandardItem* modelItem = new QStandardItem(item);
        if (item.endsWith(".h"))
            modelItem->setIcon(QIcon(":/images/header.png"));
        else if(item.endsWith(".c"))
            modelItem->setIcon(QIcon(":/images/source.png"));
        else if(item.endsWith(".S") || item.endsWith(".s"))
            modelItem->setIcon(QIcon(":/images/assembly.png"));
        else
            modelItem->setIcon(QIcon(":/images/text.png"));
        itemList << modelItem;
    }
    return itemList;
}

void ProjectModel::addFileToProject(Project *project, const QString &fileName)
{
    if(!project)
        return;

    if(fileName.isEmpty())
        return;

    FileType type;
    QStandardItem* newFileItem;
    QStandardItem* projectItem = d->projectsItemMap.value(project);
    if(!projectItem)
        return;

    if(fileName.endsWith(".c"))
    {
        type = CFILE;
        newFileItem = new QStandardItem(QIcon(":/images/source.png"),fileName);
    }
    else if (fileName.endsWith(".h"))
    {
        type = HEADERFILE;
        newFileItem = new QStandardItem(QIcon(":/images/header.png"),fileName);
    }
    else if(fileName.endsWith(".S") || fileName.endsWith(".s"))
    {
        type = ASSSEMBLYFILE;
        newFileItem = new QStandardItem(QIcon(":/images/assembly.png"),fileName);
    }
    else
    {
        type = OTHERFILE;
        newFileItem = new QStandardItem(QIcon(":/images/text.png"),fileName);
    }

    QStandardItem* folder;

    switch (type)
    {
    case CFILE:
    case ASSSEMBLYFILE:
        folder = projectItem->child(1);
        break;
    case HEADERFILE:
        folder = projectItem->child(0);
        break;
    case OTHERFILE:
        folder = projectItem->child(2);
        break;
    default:
        break;
    }

    folder->appendRow(newFileItem);
    project->updateProjectFile();
}

void ProjectModel::removeFileFromProject(Project *project, const QString &fileName)
{
    if(!project)
        return;

    if(fileName.isEmpty())
        return;

    FileType type;
    QStandardItem* projectItem = d->projectsItemMap.value(project);
    type = SoftwareDefaults::instance().fileTypeFromSuffix(fileName);

    QStandardItem* folder;

    switch (type)
    {
    case CFILE:
    case ASSSEMBLYFILE:
        folder = projectItem->child(1);
        break;
    case HEADERFILE:
        folder = projectItem->child(0);
        break;
    case OTHERFILE:
        folder = projectItem->child(2);
        break;
    default:
        break;
    }

    int childrenCount = folder->rowCount();
    for(int i = 0; i < childrenCount; i++)
        if(folder->child(i)->data(Qt::DisplayRole).toString() == fileName)
        {
            folder->removeRow(i);
            break;
        }
}

void ProjectModel::renameFileFromProject(Project *project, const QString &fileName, const QString &newName)
{
    if(!project)
        return;

    if(fileName.isEmpty())
        return;

    FileType type;
    QStandardItem* projectItem = d->projectsItemMap.value(project);
    type = SoftwareDefaults::instance().fileTypeFromSuffix(fileName);

    QStandardItem* folder;

    switch (type)
    {
    case CFILE:
    case ASSSEMBLYFILE:
        folder = projectItem->child(1);
        break;
    case HEADERFILE:
        folder = projectItem->child(0);
        break;
    case OTHERFILE:
        folder = projectItem->child(2);
        break;
    default:
        break;
    }

    int childrenCount = folder->rowCount();
    for(int i = 0; i < childrenCount; i++)
        if(folder->child(i)->data(Qt::DisplayRole).toString() == fileName)
        {
            folder->child(i)->setText(newName);
            break;
        }
}

void ProjectModel::setActiveProject(Project *project)
{
    if(!project)
        return;

    Q_FOREACH(Project* proj, d->projectsItemMap.keys())
    {
        if(proj == project)
            d->projectsItemMap.value(proj)->setBackground(QColor("#3585FF"));
        else
            d->projectsItemMap.value(proj)->setBackground(Qt::lightGray);
    }
}


void ProjectModel::updateModel()
{
}
