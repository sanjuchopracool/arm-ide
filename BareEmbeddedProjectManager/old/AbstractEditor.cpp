#include "AbstractEditor.h"
#include <QDebug>
#include <Qsci/qscistyle.h>
#include <Qsci/qscilexercpp.h>
#include <QKeyEvent>
#include <Qsci/qsciapis.h>
struct AbstractEditorData
{
    AbstractEditorData() : showLineNumber(true), lexer(0) {}
    bool showLineNumber;
    QsciLexer* lexer;
};

AbstractEditor::AbstractEditor(QWidget* parent) :
    QsciScintilla(parent)
{
    setMinimumWidth(500);

    d = new AbstractEditorData;
    d->lexer = new QsciLexerCPP(this);
    setLexer(d->lexer);
    QsciAPIs* api = new QsciAPIs(d->lexer);
    api->load("cplusplus.api");
    api->prepare();
    d->lexer->setAPIs(api);

    /*
     * Margin 0 is the margin where line number is shown
     * if lines gets more than 00 update with margin to show the linenumber
     * pressing F11 should toggle line number visibility
     */

    setMarginWidth(0, "001");
    QObject::connect(this, SIGNAL(linesChanged()), this, SLOT(updateLineNumberMargin()));

    setUtf8(true);

    //caret line related stuff
    setCaretLineVisible(true);
    setCaretLineBackgroundColor(QColor("#F8F7F6"));


    /*
     * lexer related stuff this code should be in different function where we will set the lexer
     */

    //Code folding related stuff
    setFolding(BoxedTreeFoldStyle, 1);

    //indentation related stuff
//    setIndentationGuidesBackgroundColor(Qt::black);
//    setIndentationGuidesForegroundColor(Qt::red);
    setAutoIndent(true);
    setBraceMatching(SloppyBraceMatch);


    //autocomplete related stuff
    setAutoCompletionThreshold(3);
        setAutoCompletionSource(QsciScintilla::AcsAll);
}

AbstractEditor::~AbstractEditor()
{
    delete d->lexer;
    delete d;
    d = 0;
}

void AbstractEditor::showLineNumber(bool show)
{
    d->showLineNumber = show;
    updateLineNumberMargin();
}

bool AbstractEditor::lineNumberVisible() const
{
    return d->showLineNumber;
}

void AbstractEditor::setBackgroundColor(const QColor& backgroundColor)
{
    setPaper(backgroundColor);
}

void AbstractEditor::updateLineNumberMargin()
{
    if(d->showLineNumber)
        setMarginWidth(0, QString("00") + QString::number(lines()));
    else
        setMarginWidth(0 , 0);
}

void AbstractEditor::keyPressEvent(QKeyEvent* keyPressEvent)
{
    int key = keyPressEvent->key();

    if(keyPressEvent->modifiers() & Qt::CTRL)
    {
        switch(key) {
        case Qt::Key_F:
            emit findWidgetRequested(selectedText(), false);
            return;
        case Qt::Key_R:
            emit findWidgetRequested(selectedText(), true);
            return;
        case Qt::Key_S:
            emit saveRequested(this);
            return;
        }
    }

    if(key ==Qt::Key_Escape)
        emit hideFindWidgetRequest();
    else if(key == Qt::Key_F11)
    {
        d->showLineNumber = !d->showLineNumber;
        showLineNumber(d->showLineNumber);
        return;
    }
    QsciScintilla::keyPressEvent(keyPressEvent);
}
