#ifndef CREATEFILEDIALOG_H
#define CREATEFILEDIALOG_H

#include <QDialog>
#include "ProjectData.h"
#include "Constants.h"

namespace Ui {
class CreateFileDialog;
}

class CreateFileDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit CreateFileDialog(QWidget *parent = 0,Project* project = 0, FileType type = OTHERFILE);
    ~CreateFileDialog();
    
private slots:
    void slotOkClicked();
    void slotDisableOk(const QString& str);

private:
    Ui::CreateFileDialog *ui;
    Project* m_project;
    FileType m_type;
};

#endif // CREATEFILEDIALOG_H
