#ifndef CENTRALTABWIDGET_H
#define CENTRALTABWIDGET_H

#include <QTabWidget>
#include <QVBoxLayout>
#include "AbstractEditor.h"
#include "ProjectData.h"
#include "ProjectFile.h"

struct CentralTabWidgetData;
class CentralTabWidget : public QWidget
{
    Q_OBJECT
public:
    CentralTabWidget(QWidget* parent = 0);
    ~CentralTabWidget();
    void openFile(ProjectFile* file);
    void setCurrentEditor(QWidget* widget);
    void saveFile(ProjectFile* file,AbstractEditor* editor);
    void saveFileAndClose(ProjectFile* file, bool close = false);
    void closeFile(ProjectFile* file);

private slots:
    void slotTabCloseRequested(int index);
    void slotFindWidgetReuested(const QString& searchFor, bool replaceSupport);
    void slotHide();
    void slotTextModified();
    void slotSaveRequested(AbstractEditor* editor);

private:
    CentralTabWidgetData* d;
};

#endif // CENTRALTABWIDGET_H
