#include "ProjectExplorer.h"
#include <QDebug>
#include <QContextMenuEvent>
#include <QModelIndex>
#include <QStandardItemModel>
#include <QMenu>
#include <QAction>
#include "SoftwareDefaults.h"

struct ProjectExplorerData
{
    ProjectExplorerData() {}
    QAction* setAsActiveAction;
    QAction* makeFileAction;
    QAction* buildAction;
    QAction* rebuildAction;
    QAction* cleanAction;
    QAction* addNew;
    QAction* addExistingAction;
    QAction* closeAction;
    QAction* openAction;
    QAction* removeAction;
    QAction* renameAction;
};

ProjectExplorer::ProjectExplorer(QWidget *parent) :
    QTreeView(parent)
{
    //hide headers
    setHeaderHidden(true);
    d = new ProjectExplorerData();
    d->setAsActiveAction = new QAction(tr("Set as active project"),this);
    d->makeFileAction = new QAction(tr("Generate Makefile"),this);
    d->buildAction = new QAction(tr("Build"),this);
    d->rebuildAction = new QAction(tr("Rebuild"),this);
    d->cleanAction = new QAction(tr("Clean"),this);
    d->addNew = new QAction(tr("Add new"),this);
    d->addExistingAction = new QAction(tr("Add existing files"),this);
    d->closeAction = new QAction(tr("Close project"),this);
    d->openAction = new QAction(tr("Open"),this);
    d->removeAction = new QAction(tr("Remove"),this);
    d->renameAction = new QAction(tr("Rename"),this);
    setMinimumWidth(100);

}

//ProjectExplorer &ProjectExplorer::instance()
//{
//    static ProjectExplorer theInstance;
//    return theInstance;
//}

void ProjectExplorer::contextMenuEvent(QContextMenuEvent *event)
{
    /*
     * Context menu event
     * If clicked on chops file either project file
     * Menu Items will be
     *
     * set as Active Project
     * Generate makefile
     * Build
     * Rebuild
     * Clean
     * Add new
     * Add existing
     * Close Project
     */

    /*
     * Sources files menu will be
     * Open
     * Remove
     * Rename
     */

    /*
     * Linker and startup file must always be there with same name
     * Menu will be
     * Open
     *
     */

    /*
     * For other file menu will contain
     * Open
     * Remove
     * Rename
     */

    QPoint pos = event->pos();
    QModelIndex index = indexAt(pos);
    QAbstractItemModel* fModel = model();

    FileType fileType;
    Project* project = 0;

    QString fileName = fModel->data(index,Qt::DisplayRole).toString();

    fileType = SoftwareDefaults::instance().fileTypeFromSuffix(fileName);

    if(fileType == FOLDER)
        return;

    if(fileType == PROJECTFILE)
        project = Project::projectFromVariant(fModel->data(index, Qt::UserRole));
    else
        project = Project::projectFromVariant(fModel->data(index.parent().parent(), Qt::UserRole));

    QMenu fileMenu(this);
    switch (fileType) {
    case PROJECTFILE:
        fileMenu.addAction(d->setAsActiveAction);
        fileMenu.addAction(d->makeFileAction);
        fileMenu.addSeparator();
        fileMenu.addAction(d->buildAction);
        fileMenu.addAction(d->rebuildAction);
        fileMenu.addAction(d->cleanAction);
        fileMenu.addSeparator();
        fileMenu.addAction(d->addNew);
        fileMenu.addAction(d->addExistingAction);
        fileMenu.addSeparator();
        fileMenu.addAction(d->closeAction);
        break;

    case CFILE:
    case ASSSEMBLYFILE:
    case OTHERFILE:
    case HEADERFILE:

        fileMenu.addAction(d->openAction);
        if(fileName == "startup.S")
            break;
        fileMenu.addAction(d->renameAction);
        fileMenu.addAction(d->removeAction);
        break;

    case LINKERFILE:
        fileMenu.addAction(d->openAction);
        break;

    default:
        break;
    }

    QAction* actionPerformed = fileMenu.exec(QCursor::pos());
    if(!project)
        return;
    if(actionPerformed == d->openAction)
        emit openFile(project, fileName);
    else if( actionPerformed == d->closeAction)
        emit closeProject(project);
    else if( actionPerformed == d->setAsActiveAction)
        emit setActiveProject(project);
    else if( actionPerformed == d->addNew)
        emit addNewFile(project);
    else if( actionPerformed == d->removeAction)
        emit removeFile(project, fileName);
    else if (actionPerformed == d->renameAction)
        emit renameFile(project, fileName);
    else if( actionPerformed == d->makeFileAction)
        emit generateMakeFile(project);
    else if (actionPerformed == d->cleanAction)
        emit cleanProject(project);
    else if (actionPerformed == d->buildAction)
        emit buildProject(project);
    else if (actionPerformed == d->rebuildAction)
        emit rebuildProject(project);
}

void ProjectExplorer::mouseDoubleClickEvent(QMouseEvent *event)
{
    QPoint pos = event->pos();
    QModelIndex index = indexAt(pos);
    QAbstractItemModel* fModel = model();

    FileType fileType;
    Project* project = 0;

    QString fileName = fModel->data(index,Qt::DisplayRole).toString();

    fileType = SoftwareDefaults::instance().fileTypeFromSuffix(fileName);

    if(fileType == FOLDER)
        return;

    if(fileType == PROJECTFILE)
        project = Project::projectFromVariant(fModel->data(index, Qt::UserRole));
    else
        project = Project::projectFromVariant(fModel->data(index.parent().parent(), Qt::UserRole));

    if(!project)
        return;
    emit openFile(project, fileName);
}


