#ifndef CONSTANTS_H
#define CONSTANTS_H
#include <QString>

enum FileType {
    PROJECTFILE,
    HEADERFILE,
    CFILE,
    ASSSEMBLYFILE,
    LINKERFILE,
    OTHERFILE,
    FOLDER
};

#ifdef Q_OS_UNIX
static QString slash = "/";
#else
static QString slash = "\\";
#endif

#endif // CONSTANTS_H
