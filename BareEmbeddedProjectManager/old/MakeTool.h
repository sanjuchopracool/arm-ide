#ifndef MAKETOOL_H
#define MAKETOOL_H

#include <QObject>
class Project;
class ProjectFile;

class MakeTool : public QObject
{
    Q_OBJECT
public:
    explicit MakeTool(QObject *parent = 0);
    void generateMakeFileForProject(Project* project);
    QString allTargetsForProject(Project* project);
    QString targetForEachFile(ProjectFile* file);
    QStringList searchForHeadersInFile(ProjectFile* filePtr);

    
signals:
    
public slots:
    
};

#endif // MAKETOOL_H
