#ifndef PROJECTFILE_H
#define PROJECTFILE_H
#include "Constants.h"
#include <QString>
class Project;
struct ProjectFileData;
class ProjectFile
{
    //Q_OBJECT
public:
    ProjectFile(Project* project,const QString& name);
    Project* project() const;
    void changeName(const QString& newName);
    QString name();
    FileType fileType() const;
    QString filePath();
    bool modified() const;
    void setModified(bool modify);

private:
    ProjectFileData* d;
};

#endif // PROJECTFILE_H
