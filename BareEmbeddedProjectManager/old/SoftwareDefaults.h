#ifndef SOFTWAREDEFAULTS_H
#define SOFTWAREDEFAULTS_H

#include <QObject>
#include "Constants.h"

struct SoftwareDefaultsData;
class SoftwareDefaults : public QObject
{
    Q_OBJECT
public:
    SoftwareDefaults(QObject *parent = 0);
    static SoftwareDefaults& instance() ;

    void setDefaultProjectDir(const QString& projectDir);
    QString defaultProjectDir() const;

    void setToolChainPrefix(const QString& prefix);
    QString toolChainPrefix() const;

    FileType fileTypeFromSuffix(const QString& fileName);

    void save();
    void load();

    bool defaultSettingPresent();
    
signals:
    
public slots:
    void modifiedSettings();
private:
    SoftwareDefaultsData* d;
    
};

#endif // SOFTWAREDEFAULTS_H
