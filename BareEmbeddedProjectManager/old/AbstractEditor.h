#ifndef ABSTRACTEDITOR_H
#define ABSTRACTEDITOR_H

#include <qt4/Qsci/qsciscintilla.h>

struct AbstractEditorData;
class AbstractEditor : public QsciScintilla
{
    Q_OBJECT

public:
   AbstractEditor(QWidget *parent = 0);
   ~AbstractEditor();
   void showLineNumber(bool show);
   bool lineNumberVisible() const;
   void setBackgroundColor(const QColor& backgroundColor);

public slots:
   void updateLineNumberMargin();
//   void search(const QString& searchFor,bool all,

signals:
   void findWidgetRequested(const QString& searchFor,bool replaceSupport);
   void hideFindWidgetRequest();
   void saveRequested(AbstractEditor* editor);

protected:
   virtual void keyPressEvent(QKeyEvent *keyPressEvent);
private:
   AbstractEditorData* d;
};

#endif // ABSTRACTEDITOR_H
