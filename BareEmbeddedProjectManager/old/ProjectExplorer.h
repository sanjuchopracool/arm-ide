#ifndef PROJECTEXPLORER_H
#define PROJECTEXPLORER_H

#include <QTreeView>
#include "ProjectData.h"
#include "Constants.h"

struct ProjectExplorerData;
class ProjectExplorer : public QTreeView
{
    Q_OBJECT
public:
    ProjectExplorer(QWidget *parent = 0);
 //   static ProjectExplorer& instance();

signals:
    void openFile(Project*, const QString& fileName);
    void closeProject(Project* project);
    void setActiveProject(Project* project);
    void addNewFile(Project* project);
    void removeFile(Project* project,const QString&);
    void renameFile(Project* project,const QString& fileName);
    void generateMakeFile(Project* project);
    void cleanProject(Project* project);
    void buildProject(Project* project);
    void rebuildProject(Project* project);

protected:
    void contextMenuEvent(QContextMenuEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);

private:
    ProjectExplorerData* d;

};

#endif // PROJECTEXPLORER_H
