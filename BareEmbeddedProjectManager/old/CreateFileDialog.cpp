#include "CreateFileDialog.h"
#include "ui_CreateFileDialog.h"
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include "MainWindow.h"
#include <QDebug>

CreateFileDialog::CreateFileDialog(QWidget *parent, Project *project, FileType type) :
    QDialog(parent),
    ui(new Ui::CreateFileDialog)
{
    m_type = type;
    m_project = project;
    ui->setupUi(this);
    ui->pbOk->setEnabled(false);
    this->setWindowTitle(tr("Type name for new file"));
    connect(ui->pbOk, SIGNAL(clicked()), this, SLOT(slotOkClicked()));
    connect(ui->leFileName, SIGNAL(textChanged(QString)),  this ,SLOT(slotDisableOk(QString)));
    connect(ui->pbCancel, SIGNAL(clicked()), this, SLOT(accept()));
}

CreateFileDialog::~CreateFileDialog()
{
    delete ui;
}

void CreateFileDialog::slotOkClicked()
{
    QString fileDirectory = m_project->projectPath() + slash;
    QString ext;
    QString fileBaseName = ui->leFileName->text();
    switch(m_type) {
    case HEADERFILE:
        fileDirectory += "include";
        ext = ".h";
        m_project->addHeaderFile(fileBaseName+ext);
        break;
    case CFILE:
        fileDirectory += "src";
        ext = ".c";
        m_project->addSourceFile(fileBaseName+ext);
        break;
    case ASSSEMBLYFILE:
        fileDirectory += "src";
        ext = ".S";
        m_project->addSourceFile(fileBaseName+ext);
        break;
    case OTHERFILE:
        fileDirectory += "other";
        m_project->addOtherFile(fileBaseName);
        break;
    default:
        return;
    }

    fileDirectory += slash;
    QString FileName = fileBaseName + ext;
    if(!QDir(fileDirectory).exists())
        QDir(m_project->projectPath()).mkdir(fileDirectory);
    QFile file(fileDirectory + FileName);
    if(!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::warning(this,tr("Failed to create file"),
                             tr("Failed to open the file %1 in write mode").arg(file.fileName()));
    }
    else
    {
        MainWindow* mainWindow = static_cast<MainWindow*>(m_project->parent());
        if(mainWindow)
            mainWindow->projectModel()->addFileToProject(m_project, FileName);
        file.close();
    }
    this->accept();
}

void CreateFileDialog::slotDisableOk(const QString& str)
{
    if(str.isEmpty())
        ui->pbOk->setEnabled(false);
    else
        ui->pbOk->setEnabled(true);
}
