#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDockWidget>

#include "ProjectExplorer.h"
#include "ProjectModel.h"
#include "CentralTabWidget.h"
#include "MakeTool.h"
class QPlainTextEdit;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    Project* currentProject();
    static MainWindow& instance();
    ProjectModel* projectModel();
    ProjectExplorer* projectTree();

private slots:
    void newProject();
    void openProject();
    void closeProject();
    void closeProject(Project* project, bool yesAll = false, bool close = false);
    void slotSetActiveProject(Project* project);
    void changeToolChain();
    void changeLinkerSetting();
    void changeStartUpCode();
    void changeCompilerSetting();


    //file Related Stuff;
    void slotAddNewFile( Project* project);
    void slotRemoveFile( Project* project, const QString& fileName);
    void slotOpenFile(Project *project, const QString& fileName);
    void deleteFileFromDisk(ProjectFile* file, bool showMessage = true);
    void slotRenameFile(Project* project, const QString & fileName);
    void slotGenerateMakeFile(Project* project);
    void slotCleanProject(Project* project);
    void slotBuildProject(Project* project);
    void slotRebuildProject(Project* project);

protected:
    void closeEvent(QCloseEvent *event);

private:

    void checkExternalTools();
    void createActions();
    void createMenus();
    void createDocks();

    void enableProjectMenu(bool flag);
    ////////////////////////////////////////////////

    QMenu* m_fileMenu;
    QMenu* m_projectMenu;
    QMenu* m_settingMenu;
    QMenu* m_dockMenu;

    QAction* m_newProjectAction;
    QAction* m_openProjectAction;
    QAction* m_closeCurrentProjectAction;

    QAction* m_saveFileAction;
    QAction* m_saveFileAsAction;

    QAction* m_saveAllAction;
    QAction* m_closeAllAction;

    QAction* m_closeProjectAction;
    QAction* m_recentProjects;

    QAction* m_quitAction;


    //project Actions
    QAction* m_linkerSettingAction;
    QAction* m_startUpCodeAction;
    QAction* m_compilerSetting;
    //setting Actions
    QAction* m_toolChainAction;


    /*
     *toolChain prefix for arm-elf-gcc it will be
     *arm-elf- we have to add other extension
     *like for compile arm-elf-gcc
     *for assembler arm-elf-as
     *for objcopy  arm-elf-objcoy
     */
    QString m_toolChainPrefix;
    QDockWidget* m_projectExplorerDock;
    QDockWidget* m_logDock;
    QPlainTextEdit* m_logEditor;
    ProjectModel* m_model;
    ProjectExplorer* m_explorer;

    Project* m_currentProject;
    Project* m_lastProject;
    CentralTabWidget* m_workSpace;
    QList<Project*> m_projects;


    //makeFile related tool
    MakeTool* m_makeTool;
};

#endif // MAINWINDOW_H
