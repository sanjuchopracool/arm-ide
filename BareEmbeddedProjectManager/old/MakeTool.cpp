#include "MakeTool.h"
#include <QDebug>
#include <QFile>
#include <QTextStream>
#include "ProjectData.h"
#include <QMessageBox>
#include "ProjectFile.h"
MakeTool::MakeTool(QObject *parent) :
    QObject(parent)
{
}

void MakeTool::generateMakeFileForProject(Project* project)
{
    if(!project)
        return;

    /*
     * ADD ALL THE THING TO STRING TAKE CARE OF NEWLINES AND TABS:)
     */
    QFile makeFile(project->projectPath() + slash + "Makefile");
    if(!makeFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QMessageBox::warning(qobject_cast<QWidget*>(project->parent()), tr("Failed to open Makefile"),
                tr("Failed to open Makefile in write mode.\n Please check write permission for user."));
        return;
    }
    else
    {
        QTextStream stream(&makeFile);
        QString projectName = project->projectName();
        QString projectPath = project->projectPath();
        stream << "TOOLCHAIN=" << project->toolChainPrefix() + "-\n";
        stream << "ARM_CPU=" << project->armCore() + "\n";
        stream << "VPATH= " << projectPath + slash +  "src " + projectPath + slash +  "other " +
                  projectPath + slash +  "include \n";
        stream << "INCLUDE= " + projectPath + slash +"include\n";
        stream << "CCFLAGS= -mcpu=$(ARM_CPU) " << project->compilerCFlags() + "\n";
        stream << "ASFLAGS= -mcpu=$(ARM_CPU) "  << project->assemblerFlags() + "\n\n\n";
        QString allTarget =  allTargetsForProject(project);
        stream << "all: " << allTarget + "\n";
        stream << "\t$(TOOLCHAIN)ld  -o " +
                  projectName + ".elf -T " + projectPath + slash  + "other" + slash +"ROM.ld " + allTarget + "\n";

        if(project->makeHex())
            stream << "\t$(TOOLCHAIN)objcopy -O ihex " + projectName +".elf " + projectName + ".hex \n";
        if(project->makeBin())
            stream << "\t$(TOOLCHAIN)objcopy -O binary " + projectName +".elf " + projectName + ".bin \n";

        QString eachFileTarget;
        Q_FOREACH(ProjectFile* file, project->files())
        {
           eachFileTarget = targetForEachFile(file);
           if(!eachFileTarget.isEmpty())
           {
               stream << eachFileTarget;
           }
        }

        // add clean targets
        stream << "\n\n.PHONY: clean \n";
        stream << "clean : \n";
        stream << "\trm -rf *.o  *.elf *.hex *.o *.bin \n";
        makeFile.close();
        stream.flush();
    }
}

QString MakeTool::allTargetsForProject(Project* project)
{
    if(!project)
        return QString();
    QString allTargets;

    Q_FOREACH(ProjectFile* file, project->files())
    {
        if(file->fileType() == ASSSEMBLYFILE || file->fileType() == CFILE)
        {
            allTargets += QFileInfo(file->filePath()).baseName() + ".o ";
        }
    }

    return allTargets;
}

QString MakeTool::targetForEachFile(ProjectFile* file)
{
    if(!file)
        return QString();
    QString targetStr = "\n";
    if(file->fileType() == ASSSEMBLYFILE || file->fileType() == CFILE)
    {
        QFileInfo fileInfo = QFileInfo(file->filePath());
        targetStr += fileInfo.baseName() + ".o"+ " : " +
                fileInfo.fileName() + " " + searchForHeadersInFile(file).join(" ") + "\n";

        if(file->fileType() == CFILE)
            targetStr += "\t$(TOOLCHAIN)gcc $(CCFLAGS) -I$(INCLUDE) -c ";
        else
            targetStr +=  "\t$(TOOLCHAIN)as $(ASFLAGS) -o "+ fileInfo.baseName() + ".o ";

        targetStr += "$< \n";
    }

    return targetStr;
}

QStringList MakeTool::searchForHeadersInFile(ProjectFile *filePtr)
{
    QStringList ret;
    if(!filePtr)
        return ret;

    QString fileData;
    QFile file(filePtr->filePath());
    if(!file.open(QIODevice::ReadOnly))
        qDebug() << "Failed to open" << file.fileName() << "in readonly mode";
    else
    {
        fileData = file.readAll();
        file.close();

        QRegExp regexp("#include");
        int pos = 0;
        QString headerStr;
        QChar ch;
        int index;
        while((pos = regexp.indexIn(fileData,pos)) != -1)
        {
            headerStr.clear();
            pos = pos + regexp.matchedLength();
            index = pos;
            while((ch = fileData[index]) != '\n')
            {
                index++;
                headerStr += ch;
            }

            headerStr = headerStr.trimmed();
            headerStr.remove(0,1);
            headerStr.remove(headerStr.length() -1, 1);
            if(!ret.contains(headerStr))
                ret.append(headerStr);
        }

    }

    return ret;
}
