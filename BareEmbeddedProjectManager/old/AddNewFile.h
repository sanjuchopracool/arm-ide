#ifndef ADDNEWFILE_H
#define ADDNEWFILE_H

#include <QDialog>
#include <QStandardItemModel>
#include "ProjectData.h"
namespace Ui {
class AddNewFile;
}

class AddNewFile : public QDialog
{
    Q_OBJECT
    
public:
    explicit AddNewFile(QWidget *parent = 0, Project* project = 0);
    ~AddNewFile();
    
private slots:
    void slotOkClicked();
private:
    Ui::AddNewFile *ui;
    QStandardItemModel* m_model;
    Project* m_project;
};

#endif // ADDNEWFILE_H
