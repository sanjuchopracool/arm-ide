#ifndef FINDREPLACEWIDGET_H
#define FINDREPLACEWIDGET_H

#include <QWidget>

enum SearchMode
{
    PlainText = 0,
    WholeWord
};

namespace Ui {
class FindReplaceWidget;
}

class FindReplaceWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit FindReplaceWidget(QWidget *parent = 0);
    ~FindReplaceWidget();
    void showReplaceControl(bool show);
    void setSearchText(const QString& searchFor);
    
private slots:
    void toggleAdvanceSearchControl();

private:
    Ui::FindReplaceWidget *ui;
    bool m_showAdvanceControl;
};

#endif // FINDREPLACEWIDGET_H
