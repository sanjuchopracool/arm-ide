#include "FindReplaceWidget.h"
#include "ui_FindReplaceWidget.h"
#include <QDebug>

FindReplaceWidget::FindReplaceWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FindReplaceWidget)
{
    ui->setupUi(this);
    m_showAdvanceControl = false;
    //initially hide replace and advanced things
    showReplaceControl(false);

    //combobox add items
    ui->cmbMode->addItem(tr("Plain Text"));
    ui->cmbMode->addItem(tr("Whole words"));

    ui->tlbClose->setToolTip(tr("Close"));
    ui->tlbClose->setIconSize(QSize(16,16));
    ui->tlbClose->setIcon(QIcon(":/images/close.png"));
    connect(ui->tlbClose, SIGNAL(clicked()), this, SLOT(hide()));
    connect(ui->tlbReplace, SIGNAL(clicked()), this, SLOT(toggleAdvanceSearchControl()));
}

FindReplaceWidget::~FindReplaceWidget()
{
    delete ui;
}

void FindReplaceWidget::showReplaceControl(bool show)
{
    if(show)
    {
        ui->tlbReplace->setToolTip(tr("Hide Advance Filter"));
        ui->tlbReplace->setIconSize(QSize(16,16));
        ui->tlbReplace->setIcon(QIcon(":/images/down.png"));

        ui->label_3->show();
        ui->label_2->show();
        ui->label_4->show();
        ui->cmbMode->show();
        ui->cbCase->show();
        ui->pbFindAll->show();
        ui->pbReplace->show();
        ui->pbReplaceAll->show();
        ui->leReplace->show();
    }
    else
    {
        ui->tlbReplace->setToolTip(tr("Show Advance Filter"));
        ui->tlbReplace->setIconSize(QSize(16,16));
        ui->tlbReplace->setIcon(QIcon(":/images/up.png"));

        ui->label_3->hide();
        ui->label_2->hide();
        ui->label_4->hide();
        ui->cmbMode->hide();
        ui->cbCase->hide();
        ui->pbFindAll->hide();
        ui->pbReplace->hide();
        ui->pbReplaceAll->hide();
        ui->leReplace->hide();
    }
}

void FindReplaceWidget::setSearchText(const QString& searchFor)
{
    ui->leFind->setText(searchFor);
}

void FindReplaceWidget::toggleAdvanceSearchControl()
{
    m_showAdvanceControl = !m_showAdvanceControl;
    showReplaceControl(m_showAdvanceControl);
}
