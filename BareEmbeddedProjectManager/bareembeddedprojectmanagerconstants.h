#ifndef BAREEMBEDDEDPROJECTMANAGERCONSTANTS_H
#define BAREEMBEDDEDPROJECTMANAGERCONSTANTS_H

namespace BareEmbeddedProjectManager {
namespace Constants {

const char CHOPSPROJECTCONTEXT[]     = "ChopsProject.ProjectContext";
const char CHOPSPROJECTMIMETYPE[]    = "text/x-chops-project";
const char EMBEDDED_DEVICE_TYPE[] = "Embedded.Device.Type";
const char EMBEDDED_DEVICE_ID[] = "Embedded Device";

// Project
const char CHOPSPROJECT_ID[]  = "BareEmbeddedProjectManager.BareEmbeddedProject";

//actions
const char GENERATE_MAKEFILE_ACTION[] = "ProjectExplorer.GenerateMakeFile";

} // namespace BareEmbeddedProjectManager
} // namespace Constants

#endif // BAREEMBEDDEDPROJECTMANAGERCONSTANTS_H

