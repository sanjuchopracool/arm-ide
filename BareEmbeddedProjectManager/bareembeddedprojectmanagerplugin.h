#ifndef BAREEMBEDDEDPROJECTMANAGER_H
#define BAREEMBEDDEDPROJECTMANAGER_H

#include "bareembeddedprojectmanager_global.h"

#include <extensionsystem/iplugin.h>
#include <projectexplorer/project.h>
#include <projectexplorer/projectnodes.h>
#include <QAction>

namespace BareEmbeddedProjectManager {
namespace Internal {

class BareEmbeddedProjectManagerPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtCreatorPlugin" FILE "BareEmbeddedProjectManager.json")

public:
    BareEmbeddedProjectManagerPlugin();
    ~BareEmbeddedProjectManagerPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private slots:
    void updateContextActions(ProjectExplorer::Node*node, ProjectExplorer::Project*project);
    void generateMakefile();

private:
    QAction* m_generateMakeFileAction;
    ProjectExplorer::Project* m_project;
};

} // namespace Internal
} // namespace BareEmbeddedProjectManager

#endif // BAREEMBEDDEDPROJECTMANAGER_H

