#ifndef COMMONUTILSFUNCTION_H
#define COMMONUTILSFUNCTION_H

#include <QString>
#include <QDomDocument>
#include <QDomElement>
#include <QStringList>
#include <QDomText>
#include <QVariantMap>

namespace BareEmbeddedProjectManager {
namespace   Internal {
enum BareEmbeddedFileType {
    HEADERFILE,
    CSOURCEFILE,
    ASSOURCEFILE,
    LINKERFILE,
    OTHERFILE
};
/********************************************************************
 *these structure will also be saved for using default so that always
 * we can use the default setting when creating a new project
 ***************************************************************/

const char PROJEECT_CFLAGS_KEY[] = "BareEmbeddedProject.BuildSetting.Cflags";
const char PROJEECT_ASFLAGS_KEY[] = "BareEmbeddedProject.BuildSetting.AsFlags";
const char PROJEECT_CPU_KEY[] = "BareEmbeddedProject.BuildSetting.CPU";
const char PROJEECT_MAKEBIN_KEY[] = "BareEmbeddedProject.BuildSetting.MakeBin";
const char PROJEECT_MAKEHEX_KEY[] = "BareEmbeddedProject.BuildSetting.MakeHex";
const char PROJEECT_KIT_KEY[] = "BareEmbeddedProject.BuildSetting.KitId";

struct BuildSettingParameters
{
    QString kitId;
    QString cflags;
    QString asFlags;
    QString cpu;
    bool makeBinFile;
    bool makeHexFile;

    void loadFormMap(const QVariantMap& map) {
        cflags = map.value(QLatin1String(PROJEECT_CFLAGS_KEY)).toString();
        asFlags = map.value(QLatin1String(PROJEECT_ASFLAGS_KEY)).toString();
        cpu = map.value(QLatin1String(PROJEECT_CPU_KEY)).toString();
        makeBinFile = map.value(QLatin1String(PROJEECT_MAKEBIN_KEY)).toInt();
        makeHexFile = map.value(QLatin1String(PROJEECT_MAKEHEX_KEY)).toInt();
        kitId = map.value(QLatin1String(PROJEECT_KIT_KEY)).toString();
    }

    void toMap(QVariantMap& map) const {
        map.insert(QLatin1String(PROJEECT_CFLAGS_KEY), cflags);
        map.insert(QLatin1String(PROJEECT_ASFLAGS_KEY), asFlags);
        map.insert(QLatin1String(PROJEECT_CPU_KEY), cpu);
        map.insert(QLatin1String(PROJEECT_MAKEBIN_KEY), makeBinFile);
        map.insert(QLatin1String(PROJEECT_MAKEHEX_KEY), makeHexFile);
        map.insert(QLatin1String(PROJEECT_KIT_KEY), kitId);
    }

    void save(QDomDocument& doc, QDomElement& parentE) const {
        QDomElement buildSettingE = doc.createElement(QLatin1String("BuildSetting"));

        QDomElement cFlagsE = doc.createElement(QLatin1String("CFlags"));
        cFlagsE.appendChild(doc.createTextNode(cflags));
        buildSettingE.appendChild(cFlagsE);

        QDomElement kitIdE = doc.createElement(QLatin1String("KitId"));
        kitIdE.appendChild(doc.createTextNode(kitId));
        buildSettingE.appendChild(kitIdE);

        QDomElement AsFlagsE = doc.createElement(QLatin1String("AsFlags"));
        AsFlagsE.appendChild(doc.createTextNode(asFlags));
        buildSettingE.appendChild(AsFlagsE);

        QDomElement cpuE = doc.createElement(QLatin1String("CPU"));
        cpuE.appendChild(doc.createTextNode(cpu));
        buildSettingE.appendChild(cpuE);

        QDomElement makeBinE = doc.createElement(QLatin1String("MakeBin"));
        makeBinE.appendChild(doc.createTextNode(QString::number(makeBinFile)));
        buildSettingE.appendChild(makeBinE);

        QDomElement makeHexE = doc.createElement(QLatin1String("MakeHex"));
        makeHexE.appendChild(doc.createTextNode(QString::number(makeHexFile)));
        buildSettingE.appendChild(makeHexE);

        parentE.appendChild(buildSettingE);
    }

    void load(QDomDocument& doc, QDomElement& parentE) {
        Q_UNUSED(doc)

        QDomElement buildSettingE = parentE.firstChildElement(QLatin1String("BuildSetting"));
        if(!buildSettingE.isNull())
        {
            QDomElement cFlagsE = buildSettingE.firstChildElement(QLatin1String("CFlags"));
            cflags = cFlagsE.text();

            QDomElement kitIdE = buildSettingE.firstChildElement(QLatin1String("KitId"));
            kitId = kitIdE.text();

            QDomElement AsFlagsE = buildSettingE.firstChildElement(QLatin1String("AsFlags"));
            asFlags = AsFlagsE.text();

            QDomElement cpuE = buildSettingE.firstChildElement(QLatin1String("CPU"));
            cpu = cpuE.text();

            QDomElement makeBinE = buildSettingE.firstChildElement(QLatin1String("MakeBin"));
            makeBinFile = makeBinE.text().toInt();

            QDomElement makeHexE = buildSettingE.firstChildElement(QLatin1String("MakeHex"));
            makeHexFile = makeHexE.text().toInt();
        }
    }
};

static BareEmbeddedFileType fileTypeFromSuffix(const QString& fileName)
{
    BareEmbeddedFileType fileType;
    if(fileName.endsWith(QLatin1String(".c")))
            return CSOURCEFILE;
    if(fileName.endsWith(QLatin1String(".S")) || fileName.endsWith(QLatin1String(".s")))
        fileType = ASSOURCEFILE;

    else if(fileName.endsWith(QLatin1String(".h")))
        fileType = HEADERFILE;

    else if (fileName.endsWith(QLatin1String(".ld")) ||
             fileName.endsWith(QLatin1String(".Ld")) ||
             fileName.endsWith(QLatin1String(".LD")))
        fileType = LINKERFILE;

    else
        fileType = OTHERFILE;

    return fileType;
}

static QByteArray createProjectFileContent(const BuildSettingParameters &buildParameters,
                                                                     const QStringList& allFiles)
{
    QDomDocument doc(QLatin1String("ChopsProjectFile"));
    QDomElement rootE = doc.createElement(QLatin1String("Project"));
    doc.appendChild(rootE);
    //save build Setting
    buildParameters.save(doc, rootE);

    QDomElement filesE = doc.createElement(QLatin1String("Files"));
    rootE.appendChild(filesE);

    Q_FOREACH(QString fileName, allFiles)
    {
        QDomElement fileE = doc.createElement(QLatin1String("File"));
        fileE.appendChild(doc.createTextNode(QString(fileName)));
        filesE.appendChild(fileE);
    }

    return doc.toByteArray();
}

} // namepsace internal
} // namespace BareEmbeddedProjectManager

#endif // COMMONUTILSFUNCTION_H
