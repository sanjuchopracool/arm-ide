#ifndef CHOPSPROJECTMANAGER_H
#define CHOPSPROJECTMANAGER_H

#include <projectexplorer/iprojectmanager.h>

namespace BareEmbeddedProjectManager {
namespace Internal {

class BareEmbeddedProject;
class ChopsProjectManager : public ProjectExplorer::IProjectManager
{
public:
    ChopsProjectManager();

    virtual QString mimeType() const;
    virtual ProjectExplorer::Project *openProject(const QString &fileName, QString *errorString);

    void registerProject(BareEmbeddedProject *project);
    void unregisterProject(BareEmbeddedProject *project);

private:
    QList<BareEmbeddedProject *> m_projects;
};

} // Internal
} //BareEmbeddedProjectManager

#endif // CHOPSPROJECTMANAGER_H
