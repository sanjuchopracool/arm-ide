#ifndef EMBEDDEDDEVICE_H
#define EMBEDDEDDEVICE_H

#include <projectexplorer/devicesupport/idevice.h>

using namespace ProjectExplorer;
namespace BareEmbeddedProjectManager {
namespace Internal {
class EmbeddedDevice : public ProjectExplorer::IDevice
{
public:
    EmbeddedDevice();
    QString displayType() const;
    IDeviceWidget *createWidget();
    QList<Core::Id> actionIds() const;
    QString displayNameForActionId(Core::Id actionId) const;
    void executeAction(Core::Id actionId, QWidget *parent) const;
    Ptr clone() const;

protected:
    EmbeddedDevice(const EmbeddedDevice &other);
};

#endif // EMBEDDEDDEVICE_H

} // Internal
} //BareEmbeddedProjectManager
