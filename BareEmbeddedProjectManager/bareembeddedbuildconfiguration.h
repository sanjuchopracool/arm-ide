#ifndef BAREEMBEDDEDBUILDCONFIGURATION_H
#define BAREEMBEDDEDBUILDCONFIGURATION_H

#include <projectexplorer/buildconfiguration.h>
#include <utils/pathchooser.h>
#include <projectexplorer/namedwidget.h>
#include "CommonUtilsFunction.h"
#include "bareembeddedprojectmanagerconstants.h"
#include "ui_BuildSettingConfigurationWidget.h"

using namespace ProjectExplorer;
namespace BareEmbeddedProjectManager {
namespace Internal {

class BareEmbeddedBuildConfigurationFactory;

class BareEmbeddedBuildConfiguration : public ProjectExplorer::BuildConfiguration
{
    Q_OBJECT
    friend class BareEmbeddedBuildConfigurationFactory;

public:
    explicit BareEmbeddedBuildConfiguration(ProjectExplorer::Target *parent);

    ProjectExplorer::NamedWidget *createConfigWidget();
    QString buildDirectory() const;

    QString rawBuildDirectory() const;
    void setBuildDirectory(const QString &buildDirectory);

    QVariantMap toMap() const;
    BuildType buildType() const;
    void setBuildType(const BuildType& buildType);
    void setLinkerFilePath(const QString& filePath);
    BuildSettingParameters buildSettingParameters() const;

    void createMakeSteps();

    QString toolChainPrefix() const;
    QString makeToolPath() const;
    QString linkerFilePath() const;

public slots:
    void setBuildParametes(const BuildSettingParameters& buildSettingParameters);
    void setToolChainPrefix(const QString&  toolChainPrefix);
    void setMakeToolPath(const QString& makeToolPath);

    //buildParams
    void setCFlags(const QString&  cFlags);
    void setAssemblyFlags(const QString& asFlags);
    void setCpuTarget(const QString&  cpu);
    void setBuildBinFile(bool val);
    void setBuildHexFile(bool val);

protected:
    BareEmbeddedBuildConfiguration(ProjectExplorer::Target *parent, BareEmbeddedBuildConfiguration *source);
    BareEmbeddedBuildConfiguration(ProjectExplorer::Target *parent, const Core::Id id);
    virtual bool fromMap(const QVariantMap &map);

private:
    QString m_buildDirectory;
    BuildType m_buildType;
    BuildSettingParameters m_buildParameters;
    QString m_linkerFilePath;
    QString m_toolChainPrefix;
    QString m_makeToolPath;
};


//special widget i made

class BuildSettingConfigurationWidget : public QWidget
{
    Q_OBJECT

public:
    BuildSettingConfigurationWidget(QWidget *parent, BareEmbeddedBuildConfiguration* bc);
    ~BuildSettingConfigurationWidget();

private slots:

private:
    BareEmbeddedBuildConfiguration* m_bc;
    Ui::BuildSettingConfigurationWidget* ui;
};

class BareEmbeddedBuildConfigurationFactory : public ProjectExplorer::IBuildConfigurationFactory
{
    Q_OBJECT

public:
    explicit BareEmbeddedBuildConfigurationFactory(QObject *parent = 0);
    ~BareEmbeddedBuildConfigurationFactory();

    QList<Core::Id> availableCreationIds(const ProjectExplorer::Target *parent) const;
    QString displayNameForId(const Core::Id id) const;

    bool canCreate(const ProjectExplorer::Target *parent, const Core::Id id) const;
    ProjectExplorer::BuildConfiguration *create(ProjectExplorer::Target *parent, const Core::Id id, const QString &name = QString());
    bool canClone(const ProjectExplorer::Target *parent, ProjectExplorer::BuildConfiguration *source) const;
    ProjectExplorer::BuildConfiguration *clone(ProjectExplorer::Target *parent, ProjectExplorer::BuildConfiguration *source);
    bool canRestore(const ProjectExplorer::Target *parent, const QVariantMap &map) const;
    ProjectExplorer::BuildConfiguration *restore(ProjectExplorer::Target *parent, const QVariantMap &map);

private:
    bool canHandle(const ProjectExplorer::Target *t) const;
};

class ProjectBuildSettingPage;
class BareEmbeddedBuildSettingsWidget : public ProjectExplorer::NamedWidget
{
    Q_OBJECT

public:
    BareEmbeddedBuildSettingsWidget(BareEmbeddedBuildConfiguration *bc);
    ~BareEmbeddedBuildSettingsWidget();

private slots:
    void buildDirectoryChanged();

private:
    Utils::PathChooser *m_pathChooser;
    BareEmbeddedBuildConfiguration *m_buildConfiguration;
};

} // Internal
} //BareEmbeddedProjectManager
#endif // BAREEMBEDDEDBUILDCONFIGURATION_H
