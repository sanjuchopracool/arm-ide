#include "bareembeddedbuildconfiguration.h"
#include "bareembeddedprojectwizard.h"
#include "bareembeddedproject.h"
#include <projectexplorer/buildsteplist.h>
#include <projectexplorer/kitinformation.h>
#include <projectexplorer/projectexplorerconstants.h>
#include <projectexplorer/toolchain.h>
#include <projectexplorer/target.h>
#include <utils/pathchooser.h>
#include <QInputDialog>
#include <utils/qtcassert.h>
#include "bareembeddedmakestep.h"

namespace BareEmbeddedProjectManager {
namespace Internal {

const char EMBEDDED_BC_ID[] = "ChopsProjectManager.BareEmbeddedBuildConfiguration";
const char BUILD_DIRECTORY_KEY[] = "ChopsProjectManager.BareEmbeddedBuildConfiguration.BuildDirectory";
const char BUILD_TYPE_KEY[] = "ChopsProjectManager.BareEmbeddedBuildConfiguration.BuildType";
const char LINKER_FILE_KEY[] = "ChopsProjectManager.BareEmbeddedBuildConfiguration.LinkerFile";
const char MAKE_TOOL_KEY[] = "ChopsProjectManager.BareEmbeddedBuildConfiguration.MakeTool";
const char TOOLCHAIN_KEY[] = "ChopsProjectManager.BareEmbeddedBuildConfiguration.ToolChain";

/**********************************************************************
 * BuidlSettingWidget
 ***********************************************************************/

BareEmbeddedBuildSettingsWidget::BareEmbeddedBuildSettingsWidget(BareEmbeddedBuildConfiguration *bc):
    m_buildConfiguration(0)
{
    QFormLayout *fl = new QFormLayout(this);
    fl->setContentsMargins(0, -1, 0, -1);
    fl->setFieldGrowthPolicy(QFormLayout::ExpandingFieldsGrow);

    // build directory
    m_pathChooser = new Utils::PathChooser(this);
    m_pathChooser->setEnabled(true);
    fl->addRow(tr("Build directory:"), m_pathChooser);
    connect(m_pathChooser, SIGNAL(changed(QString)), this, SLOT(buildDirectoryChanged()));

    BuildSettingConfigurationWidget* configWidget = new BuildSettingConfigurationWidget(this, bc);
    fl->addRow(tr("Build Setting"), configWidget);
    m_buildConfiguration = bc;
    m_pathChooser->setBaseDirectory(bc->target()->project()->projectDirectory());
    m_pathChooser->setPath(m_buildConfiguration->rawBuildDirectory());
    setDisplayName(tr("Bare Embedded Manager"));
}

BareEmbeddedBuildSettingsWidget::~BareEmbeddedBuildSettingsWidget()
{

}

void BareEmbeddedBuildSettingsWidget::buildDirectoryChanged()
{
    m_buildConfiguration->setBuildDirectory(m_pathChooser->rawPath());
}


/***************************************************************************
 * BuildConfiguration Factory
 ***************************************************************************/

BareEmbeddedBuildConfigurationFactory::BareEmbeddedBuildConfigurationFactory(QObject *parent)
{

}

BareEmbeddedBuildConfigurationFactory::~BareEmbeddedBuildConfigurationFactory()
{

}

QList<Core::Id> BareEmbeddedBuildConfigurationFactory::availableCreationIds(const Target *parent) const
{
    if (!canHandle(parent))
        return QList<Core::Id>();
    return QList<Core::Id>() << Core::Id(EMBEDDED_BC_ID);
}

QString BareEmbeddedBuildConfigurationFactory::displayNameForId(const Core::Id id) const
{
    if (id == EMBEDDED_BC_ID)
        return tr("Build");
    return QString();
}

bool BareEmbeddedBuildConfigurationFactory::canCreate(const Target *parent, const Core::Id id) const
{
    if (!canHandle(parent))
        return false;
    if (id == EMBEDDED_BC_ID)
        return true;
    return false;
}

BuildConfiguration *BareEmbeddedBuildConfigurationFactory::create(Target *parent, const Core::Id id, const QString &name)
{
    if (!canCreate(parent, id))
        return 0;

    //TODO asking for name is duplicated everywhere, but maybe more
    // wizards will show up, that incorporate choosing the nam
    bool ok = true;
    QString buildConfigurationName = name;
    if (buildConfigurationName.isNull())
        buildConfigurationName = QInputDialog::getText(0,
                                                       tr("New Configuration"),
                                                       tr("New configuration name:"),
                                                       QLineEdit::Normal,
                                                       QString(), &ok);
    buildConfigurationName = buildConfigurationName.trimmed();
    if (!ok || buildConfigurationName.isEmpty())
        return 0;

    BareEmbeddedBuildConfiguration *bc = new BareEmbeddedBuildConfiguration(parent);
    bc->setDisplayName(buildConfigurationName);
    bc->createMakeSteps();
    return bc;
}

bool BareEmbeddedBuildConfigurationFactory::canClone(const Target *parent, BuildConfiguration *source) const
{
    return canCreate(parent, source->id());
}

BuildConfiguration *BareEmbeddedBuildConfigurationFactory::clone(Target *parent, BuildConfiguration *source)
{
    if (!canClone(parent, source))
        return 0;
    return new BareEmbeddedBuildConfiguration(parent, qobject_cast<BareEmbeddedBuildConfiguration *>(source));
}

bool BareEmbeddedBuildConfigurationFactory::canRestore(const Target *parent, const QVariantMap &map) const
{
    return canCreate(parent, ProjectExplorer::idFromMap(map));
}

BuildConfiguration *BareEmbeddedBuildConfigurationFactory::restore(Target *parent, const QVariantMap &map)
{
    if (!canRestore(parent, map))
        return 0;
    BareEmbeddedBuildConfiguration *bc(new BareEmbeddedBuildConfiguration(parent));
    if (bc->fromMap(map))
        return bc;
    delete bc;
    return 0;
}

bool BareEmbeddedBuildConfigurationFactory::canHandle(const Target *t) const
{
    if (!t->project()->supportsKit(t->kit()))
        return false;
    return qobject_cast<BareEmbeddedProject *>(t->project());
}

/**************************************************************************
 * BareEmbeddedBuildConfiguration
 ***************************************************************************/

BareEmbeddedBuildConfiguration::BareEmbeddedBuildConfiguration(Target *parent)
    : BuildConfiguration(parent, Core::Id(EMBEDDED_BC_ID)), m_buildType(Release)
{
}

BareEmbeddedBuildConfiguration::BareEmbeddedBuildConfiguration(Target *parent, BareEmbeddedBuildConfiguration *source) :
    BuildConfiguration(parent, source),
    m_buildDirectory(source->m_buildDirectory),
    m_buildType(Release)
{
    cloneSteps(source);
}

BareEmbeddedBuildConfiguration::BareEmbeddedBuildConfiguration(Target *parent, const Core::Id id)
    : BuildConfiguration(parent, id),
      m_buildType(Release)
{

}

NamedWidget *BareEmbeddedBuildConfiguration::createConfigWidget()
{
    return new BareEmbeddedBuildSettingsWidget(this);
}

QString BareEmbeddedBuildConfiguration::buildDirectory() const
{
    // Convert to absolute path when necessary
    const QDir projectDir(target()->project()->projectDirectory());
    return projectDir.absoluteFilePath(m_buildDirectory);
}

QString BareEmbeddedBuildConfiguration::rawBuildDirectory() const
{
    return m_buildDirectory;
}

void BareEmbeddedBuildConfiguration::setBuildDirectory(const QString &buildDirectory)
{
    if (m_buildDirectory == buildDirectory)
        return;
    m_buildDirectory = buildDirectory;
    emit buildDirectoryChanged();
}

QVariantMap BareEmbeddedBuildConfiguration::toMap() const
{
    QVariantMap map(BuildConfiguration::toMap());
    map.insert(QLatin1String(BUILD_DIRECTORY_KEY), m_buildDirectory);
    map.insert(QLatin1String(BUILD_TYPE_KEY), m_buildType);
    map.insert(QLatin1String(LINKER_FILE_KEY), m_linkerFilePath);
    map.insert(QLatin1String(TOOLCHAIN_KEY), m_toolChainPrefix);
    map.insert(QLatin1String(MAKE_TOOL_KEY), m_makeToolPath);
    m_buildParameters.toMap(map);
    return map;
}

BuildConfiguration::BuildType BareEmbeddedBuildConfiguration::buildType() const
{
    return m_buildType;
}

void BareEmbeddedBuildConfiguration::setBuildType(const BuildConfiguration::BuildType &buildType)
{
    m_buildType = buildType;
}

void BareEmbeddedBuildConfiguration::setLinkerFilePath(const QString &filePath)
{
    m_linkerFilePath = filePath;
}

void BareEmbeddedBuildConfiguration::setBuildParametes(const BuildSettingParameters& buildParameters)
{
    m_buildParameters = buildParameters;
}

BuildSettingParameters BareEmbeddedBuildConfiguration::buildSettingParameters() const
{
    return m_buildParameters;
}

void BareEmbeddedBuildConfiguration::createMakeSteps()
{
    BuildStepList *buildSteps = this->stepList(ProjectExplorer::Constants::BUILDSTEPS_BUILD);
    BuildStepList *cleanSteps = this->stepList(ProjectExplorer::Constants::BUILDSTEPS_CLEAN);

    Q_ASSERT(buildSteps);
    BareEmbeddedMakeStep *makeStep = new BareEmbeddedMakeStep(buildSteps);
    buildSteps->insertStep(0, makeStep);
    makeStep->setBuildTarget(QLatin1String("all"), /* on = */ true);

    Q_ASSERT(cleanSteps);
    BareEmbeddedMakeStep *cleanMakeStep = new BareEmbeddedMakeStep(cleanSteps);
    cleanSteps->insertStep(0, cleanMakeStep);
    cleanMakeStep->setBuildTarget(QLatin1String("clean"), /* on = */ true);
    cleanMakeStep->setClean(true);
}

void BareEmbeddedBuildConfiguration::setToolChainPrefix(const QString &toolChainPrefix)
{
    m_toolChainPrefix = toolChainPrefix;
}

QString BareEmbeddedBuildConfiguration::toolChainPrefix() const
{
    return m_toolChainPrefix;
}

void BareEmbeddedBuildConfiguration::setMakeToolPath(const QString &makeToolPath)
{
    m_makeToolPath = makeToolPath;
}

void BareEmbeddedBuildConfiguration::setCFlags(const QString &cFlags)
{
    m_buildParameters.cflags = cFlags;
}

void BareEmbeddedBuildConfiguration::setAssemblyFlags(const QString &asFlags)
{
    m_buildParameters.asFlags = asFlags;
}

void BareEmbeddedBuildConfiguration::setCpuTarget(const QString &cpu)
{
    m_buildParameters.cpu = cpu;
}

void BareEmbeddedBuildConfiguration::setBuildBinFile(bool val)
{
    m_buildParameters.makeBinFile = val;
}

void BareEmbeddedBuildConfiguration::setBuildHexFile(bool val)
{
    m_buildParameters.makeHexFile = val;
}

QString BareEmbeddedBuildConfiguration::makeToolPath() const
{
    return m_makeToolPath;
}

QString BareEmbeddedBuildConfiguration::linkerFilePath() const
{
    return m_linkerFilePath;
}


bool BareEmbeddedBuildConfiguration::fromMap(const QVariantMap &map)
{
    m_buildDirectory = map.value(QLatin1String(BUILD_DIRECTORY_KEY), target()->project()->projectDirectory()).toString();
    m_buildType= (BuildType)map.value(QLatin1String(BUILD_TYPE_KEY), Release).toInt();
    m_linkerFilePath = map.value(QLatin1String(LINKER_FILE_KEY)).toString();
    m_makeToolPath = map.value(QLatin1String(MAKE_TOOL_KEY)).toString();
    m_toolChainPrefix = map.value(QLatin1String(TOOLCHAIN_KEY)).toString();
    m_buildParameters.loadFormMap(map);
    return BuildConfiguration::fromMap(map);
}

BuildSettingConfigurationWidget::BuildSettingConfigurationWidget(QWidget *parent, BareEmbeddedBuildConfiguration *bc) :
    QWidget(parent), m_bc(bc)
{
    ui = new Ui::BuildSettingConfigurationWidget;
    ui->setupUi(this);
    ui->leMakeToolPath->setText(bc->makeToolPath());
    ui->leToolChainPrefix->setText(bc->toolChainPrefix());
    BuildSettingParameters parameters = bc->buildSettingParameters();
    ui->leAssemblerFlags->setText(parameters.asFlags);
    ui->leCFlags->setText(parameters.cflags);
    ui->leCPU->setText(parameters.cpu);
    ui->checkBoxBin->setChecked(parameters.makeBinFile);
    ui->checkBoxHex->setChecked(parameters.makeHexFile);

    connect(ui->leMakeToolPath, SIGNAL(textChanged(QString)), m_bc, SLOT(setMakeToolPath(QString)));
    connect(ui->leToolChainPrefix, SIGNAL(textChanged(QString)), m_bc, SLOT(setToolChainPrefix(QString)));
    connect(ui->leCFlags, SIGNAL(textChanged(QString)), m_bc, SLOT(setCFlags(QString)));
    connect(ui->leAssemblerFlags, SIGNAL(textChanged(QString)), m_bc, SLOT(setAssemblyFlags(QString)));
    connect(ui->leCPU, SIGNAL(textChanged(QString)), m_bc, SLOT(setCpuTarget(QString)));
    connect(ui->checkBoxBin, SIGNAL(toggled(bool)), bc, SLOT(setBuildBinFile(bool)));
    connect(ui->checkBoxHex, SIGNAL(toggled(bool)), bc, SLOT(setBuildHexFile(bool)));
}

BuildSettingConfigurationWidget::~BuildSettingConfigurationWidget()
{
    delete ui;
}


} // Internal
} //BareEmbeddedProjectManager
