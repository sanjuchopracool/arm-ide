#include "bareembeddedprojectmanagerplugin.h"
#include "bareembeddedprojectmanagerconstants.h"

#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/mimedatabase.h>
#include <projectexplorer/projectexplorerconstants.h>
#include <projectexplorer/projectexplorer.h>

#include <QAction>
#include <QMessageBox>
#include <QMainWindow>
#include <QMenu>

#include <QtPlugin>
#include "bareembeddedprojectwizard.h"
#include "chopsprojectmanager.h"
#include "embeddeddevicefactory.h"
#include "bareembeddedbuildconfiguration.h"
#include "bareembeddedmakestep.h"
#include "bareembeddedprojectmanagerconstants.h"
#include "bareembeddedproject.h"
#include <QDebug>

using namespace BareEmbeddedProjectManager::Internal;

BareEmbeddedProjectManagerPlugin::BareEmbeddedProjectManagerPlugin()
{
    // Create your members
}

BareEmbeddedProjectManagerPlugin::~BareEmbeddedProjectManagerPlugin()
{
    // Unregister objects from the plugin manager's object pool
    // Delete members
}

bool BareEmbeddedProjectManagerPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    // Register objects in the plugin manager's object pool
    // Load settings
    // Add actions to menus
    // Connect to other plugins' signals
    // In the initialize method, a plugin can be sure that the plugins it
    // depends on have initialized their members.


    //menus
    Core::ActionContainer *mproject =
            Core::ActionManager::actionContainer(ProjectExplorer::Constants::M_PROJECTCONTEXT);
    Core::ActionContainer *msubproject =
            Core::ActionManager::actionContainer(ProjectExplorer::Constants::M_SUBPROJECTCONTEXT);

    Core::Context projectTreeContext(BareEmbeddedProjectManager::Constants::CHOPSPROJECTCONTEXT);

    ProjectExplorer::ProjectExplorerPlugin * projectExplorer = ProjectExplorer::ProjectExplorerPlugin::instance();
    connect(projectExplorer, SIGNAL(currentNodeChanged(ProjectExplorer::Node*,ProjectExplorer::Project*)),
            this, SLOT(updateContextActions(ProjectExplorer::Node*,ProjectExplorer::Project*)));

    Q_UNUSED(arguments)
    using namespace Core;

    Core::MimeDatabase *mimeDB = ICore::mimeDatabase();

    const QLatin1String mimetypesXml(":./BareEmbeddedProjectManager.mimetypes.xml");

    if (! mimeDB->addMimeTypes(mimetypesXml, errorString))
        return false;

    m_generateMakeFileAction = new QAction(tr("generate makefile"), this);
    Core::Command *command = Core::ActionManager::registerAction(m_generateMakeFileAction,
                                                             BareEmbeddedProjectManager::Constants::GENERATE_MAKEFILE_ACTION,
                                                             projectTreeContext);
    command->setAttribute(Core::Command::CA_Hide);
    mproject->addAction(command, ProjectExplorer::Constants::G_PROJECT_BUILD);
    msubproject->addAction(command, ProjectExplorer::Constants::G_PROJECT_BUILD);
    connect(m_generateMakeFileAction, SIGNAL(triggered()), this, SLOT(generateMakefile()));
//    m_generateMakeFileAction->s

//    Core::ActionContainer *menu = Core::ActionManager::createMenu(Constants::MENU_ID);
//    menu->menu()->setTitle(tr("BareEmbeddedProjectManager"));
//    menu->addAction(cmd);
//    Core::ActionManager::actionContainer(Core::Constants::M_TOOLS)->addMenu(menu);

    addAutoReleasedObject(new ChopsProjectManager);
    addAutoReleasedObject(new BareEmbeddedProjectWizard);
//    addAutoReleasedObject(new EmbeddedDeviceFactory);
    addAutoReleasedObject(new BareEmbeddedMakeStepFactory);
    addAutoReleasedObject(new BareEmbeddedBuildConfigurationFactory);

    return true;
}

void BareEmbeddedProjectManagerPlugin::extensionsInitialized()
{
    // Retrieve objects from the plugin manager's object pool
    // In the extensionsInitialized method, a plugin can be sure that all
    // plugins that depend on it are completely initialized.
}

ExtensionSystem::IPlugin::ShutdownFlag BareEmbeddedProjectManagerPlugin::aboutToShutdown()
{
    // Save settings
    // Disconnect from signals that are not needed during shutdown
    // Hide UI (if you add UI that is not in the main window directly)
    return SynchronousShutdown;
}

void BareEmbeddedProjectManagerPlugin::updateContextActions(Node *node, Project *project)
{
    Q_UNUSED(node)
    m_project = project;
}

void BareEmbeddedProjectManagerPlugin::generateMakefile()
{
    BareEmbeddedProject* project = qobject_cast<BareEmbeddedProject*>(m_project);
    if(project)
        project->generateMakeFile();
}

Q_EXPORT_PLUGIN2(BareEmbeddedProjectManager, BareEmbeddedProjectManagerPlugin)

