#ifndef BAREEMBEDDEDPROJECTWIZARD_H
#define BAREEMBEDDEDPROJECTWIZARD_H
#include <coreplugin/basefilewizard.h>
#include <coreplugin/generatedfile.h>
#include <utils/wizard.h>
#include <utils/filewizardpage.h>
#include "ui_CompilerConfigurationPage.h"
#include "ui_ToolChainHelpDialog.h"
#include "ui_LinkerConfigPage.h"
#include "ui_StartUpFilePage.h"
#include "CommonUtilsFunction.h"

namespace BareEmbeddedProjectManager {
namespace Internal {

/*************************************
 * ToolChainHelpDialog
 *************************************/

class ToolChainHelpDialog : public QDialog
{
    Q_OBJECT

public:
    ToolChainHelpDialog(QWidget *parent);
    ~ToolChainHelpDialog();

private:
    Ui::ToolChainHelpDialog* m_ui;
};

/*************************************
 * ProjectBuildSettingPage
 *************************************/

struct ProjectBuildSettingPageData
{
    ProjectBuildSettingPageData() : ui(0) {}
    Ui::CompilerConfigurationPage* ui;
};

class ProjectBuildSettingPage : public QWizardPage
{
    Q_OBJECT

public:
    ProjectBuildSettingPage(QWidget *parent);
    ~ProjectBuildSettingPage();

    BuildSettingParameters buildParameters() const;
    void setBuildParameters(const BuildSettingParameters& parameters);

private slots:
    void showHelpDialog();

private:
    ProjectBuildSettingPageData* d;
};


//Qdialog to select a tool kit

class KitSelectionDialog : public QDialog
{
    Q_OBJECT

public:
    KitSelectionDialog(QWidget* parent);
    ~KitSelectionDialog();

    BuildSettingParameters buildParameters() const;
    void setBuildParameters(const BuildSettingParameters& parameters);

private slots:
    void showHelpDialog();

private:
    ProjectBuildSettingPageData* d;
};

/*************************************
 * ProjectLinkerSettingPage
 *************************************/

struct LinkerParameter
{
    QString romStartAddress;
    QString romsize;
    QString ramStartAddress;
    QString ramSize;
};

class LinkerSettingPage : public QWizardPage
{
    Q_OBJECT

public:
    LinkerSettingPage(QWidget* parent);
    ~LinkerSettingPage();

    LinkerParameter linkerParameters();
    void setLinkerParameters(const LinkerParameter& parameters);
    QString linkerString() const;

private:
    Ui::LinkerConfigPage* m_ui;
};

/*************************************
 * StartUp File SettingPage
 *************************************/

struct startUpFileParameter
{
    QString undStack;
    QString irqStack;
    QString fiqStack;
    QString svcStack;
    QString abrtStack;
    QString sysStack;
};

class StartUpFileSettingPage : public QWizardPage
{
    Q_OBJECT

public:
    StartUpFileSettingPage(QWidget* parent);
    ~StartUpFileSettingPage();

    void setStartUpFileParameters(const startUpFileParameter& parameters);
    QString startUpString() const;

private:
    Ui::StartUpFilePage* m_ui;
};


/*************************************
 * BareEmbeddedProjectWizardDialog
 *************************************/

struct BareEmbeddedProjectWizardData
{
    BareEmbeddedProjectWizardData() :
        projectNameLocationPage(0),
        projectBuildSetting(0),
        linkerSettingPage(0),
        startUpSettingPage(0)
    {}

    Utils::FileWizardPage* projectNameLocationPage;
    ProjectBuildSettingPage* projectBuildSetting;
    LinkerSettingPage* linkerSettingPage;
    StartUpFileSettingPage* startUpSettingPage;
};
class BareEmbeddedProjectWizardDialog : public Utils::Wizard
{
    Q_OBJECT

public:
    BareEmbeddedProjectWizardDialog(QWidget *parent = 0);
    ~BareEmbeddedProjectWizardDialog();

    //first page project name and path
    QString path() const;
    void setPath(const QString &path);
    QString projectName() const;

    //2nd Page
    BuildSettingParameters buildParameters() const;

    //3rd page can give complete linker string
    QString linkerString() const;

    //4th page
    QString startUpString() const;
private:
    BareEmbeddedProjectWizardData* d;
};


class BareEmbeddedProjectWizard : public Core::BaseFileWizard
{
    Q_OBJECT
public:
    BareEmbeddedProjectWizard();

    static Core::BaseFileWizardParameters parameters();

protected:
    QWizard *createWizardDialog(QWidget *parent,
                                        const Core::WizardDialogParameters &wizardDialogParameters) const;

    Core::GeneratedFiles generateFiles(const QWizard *w,
                                         QString *errorMessage) const;

    bool postGenerateFiles(const QWizard *w, const Core::GeneratedFiles &l, QString *errorMessage);

};

}
}

#endif // BAREEMBEDDEDPROJECTWIZARD_H
