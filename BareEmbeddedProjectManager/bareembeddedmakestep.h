#ifndef BAREEMBEDDEDMAKESTEP_H
#define BAREEMBEDDEDMAKESTEP_H

#include <projectexplorer/abstractprocessstep.h>
#include <projectexplorer/target.h>
#include  "ui_BareEmbeddedMakeStepWidget.h"

QT_BEGIN_NAMESPACE
class QListWidgetItem;
QT_END_NAMESPACE

namespace BareEmbeddedProjectManager {
namespace Internal {

class BareEmbeddedMakeStepConfigWidget;
class BareEmbeddedMakeStepFactory;

class BareEmbeddedMakeStep : public ProjectExplorer::AbstractProcessStep
{
    Q_OBJECT

    friend class BareEmbeddedMakeStepConfigWidget;
    friend class BareEmbeddedMakeStepFactory;

public:
    BareEmbeddedMakeStep(ProjectExplorer::BuildStepList *parent);
    ~BareEmbeddedMakeStep();

    bool init();
    void run(QFutureInterface<bool> &fi);

    ProjectExplorer::BuildStepConfigWidget *createConfigWidget();
    bool immutable() const;
    bool buildsTarget(const QString &target) const;
    void setBuildTarget(const QString &target, bool on);
    QString allArguments() const;
    QString makeCommand(const Utils::Environment &environment) const;

    void setClean(bool clean);
    bool isClean() const;

    QVariantMap toMap() const;

protected:
    BareEmbeddedMakeStep(ProjectExplorer::BuildStepList *parent, BareEmbeddedMakeStep *bs);
    BareEmbeddedMakeStep(ProjectExplorer::BuildStepList *parent, const Core::Id id);
    bool fromMap(const QVariantMap &map);

private:
    void ctor();

    QStringList m_buildTargets;
    QString m_makeArguments;
    QString m_makeCommand;
    bool m_clean;
    QList<ProjectExplorer::Task> m_tasks;
};

class BareEmbeddedMakeStepConfigWidget : public ProjectExplorer::BuildStepConfigWidget
{
    Q_OBJECT

public:
    BareEmbeddedMakeStepConfigWidget(BareEmbeddedMakeStep *makeStep);
    ~BareEmbeddedMakeStepConfigWidget();
    QString displayName() const;
    QString summaryText() const;

private slots:
    void itemChanged(QListWidgetItem *item);
    void makeLineEditTextEdited();
    void makeArgumentsLineEditTextEdited();
    void updateMakeOverrrideLabel();
    void updateDetails();

private:
    Ui::BareEmbeddedMakeStepWidget* m_ui;
    BareEmbeddedMakeStep *m_makeStep;
    QString m_summaryText;
};

class BareEmbeddedMakeStepFactory : public ProjectExplorer::IBuildStepFactory
{
    Q_OBJECT

public:
    explicit BareEmbeddedMakeStepFactory(QObject *parent = 0);

    bool canCreate(ProjectExplorer::BuildStepList *parent, const Core::Id id) const;
    ProjectExplorer::BuildStep *create(ProjectExplorer::BuildStepList *parent, const Core::Id id);
    bool canClone(ProjectExplorer::BuildStepList *parent,
                  ProjectExplorer::BuildStep *source) const;
    ProjectExplorer::BuildStep *clone(ProjectExplorer::BuildStepList *parent,
                                      ProjectExplorer::BuildStep *source);
    bool canRestore(ProjectExplorer::BuildStepList *parent, const QVariantMap &map) const;
    ProjectExplorer::BuildStep *restore(ProjectExplorer::BuildStepList *parent,
                                        const QVariantMap &map);

    QList<Core::Id> availableCreationIds(ProjectExplorer::BuildStepList *bc) const;
    QString displayNameForId(const Core::Id id) const;
};

} //Internal
} //BareEmbeddedProjectManager
#endif // BAREEMBEDDEDMAKESTEP_H
