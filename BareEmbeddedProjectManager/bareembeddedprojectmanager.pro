DEFINES += BAREEMBEDDEDPROJECTMANAGER_LIBRARY

# BareEmbeddedProjectManager files
QT += xml

SOURCES += bareembeddedprojectmanagerplugin.cpp \
    bareembeddedprojectwizard.cpp \
    chopsprojectmanager.cpp \
    bareembeddedproject.cpp \
    bareembeddedprojectfileeditor.cpp \
    bareembeddedprojectnode.cpp \
    embeddeddevicefactory.cpp \
    embeddeddevice.cpp \
    bareembeddedbuildconfiguration.cpp \
    bareembeddedmakestep.cpp

HEADERS += bareembeddedprojectmanagerplugin.h \
        bareembeddedprojectmanager_global.h \
        bareembeddedprojectmanagerconstants.h \
    bareembeddedprojectwizard.h \
    chopsprojectmanager.h \
    bareembeddedproject.h \
    bareembeddedprojectfileeditor.h \
    bareembeddedprojectnode.h \
    CommonUtilsFunction.h \
    embeddeddevicefactory.h \
    embeddeddevice.h \
    bareembeddedbuildconfiguration.h \
    bareembeddedmakestep.h

# Qt Creator linking

## set the QTC_SOURCE environment variable to override the setting here
QTCREATOR_SOURCES = $$(QTC_SOURCE)
isEmpty(QTCREATOR_SOURCES):QTCREATOR_SOURCES=/home/sanju/SoftwareHack/qt-creator-2.8.1-src

## set the QTC_BUILD environment variable to override the setting here
IDE_BUILD_TREE = $$(QTC_BUILD)
isEmpty(IDE_BUILD_TREE):IDE_BUILD_TREE=/home/sanju/SoftwareHack/build-qtcreator-Desktop-Debug

## uncomment to build plugin into user config directory
## <localappdata>/plugins/<ideversion>
##    where <localappdata> is e.g.
##    "%LOCALAPPDATA%\QtProject\qtcreator" on Windows Vista and later
##    "$XDG_DATA_HOME/data/QtProject/qtcreator" or "~/.local/share/data/QtProject/qtcreator" on Linux
##    "~/Library/Application Support/QtProject/Qt Creator" on Mac
# USE_USER_DESTDIR = yes

PROVIDER = ChopsGNU

include($$QTCREATOR_SOURCES/src/qtcreatorplugin.pri)

FORMS += \
    ToolChainHelpDialog.ui \
    CompilerConfigurationPage.ui \
    LinkerConfigPage.ui \
    StartUpFilePage.ui \
    BareEmbeddedMakeStepWidget.ui \
    BuildSettingConfigurationWidget.ui

OTHER_FILES += \
    BareEmbeddedProjectManager.mimetypes.xml \
    linkerString \
    StartUpFileString \
    AsmHeaderString

RESOURCES += \
    BareEmbeddedProjectManager.qrc

