#include "bareembeddedprojectwizard.h"
#include <projectexplorer/projectexplorer.h>
#include <QPixmap>
#include <QPainter>
#include <QApplication>
#include <QStyle>
#include <QDir>
#include <projectexplorer/customwizard/customwizard.h>
#include <QFile>
#include <projectexplorer/kitinformation.h>
#include <projectexplorer/kitmanager.h>

namespace BareEmbeddedProjectManager {
namespace Internal {

/*************************************
 * ToolChainHelpDialog
 *************************************/

ToolChainHelpDialog::ToolChainHelpDialog(QWidget *parent) :
    QDialog(parent)
{
    m_ui = new Ui::ToolChainHelpDialog;
    m_ui->setupUi(this);
    setWindowTitle(tr("Configure project compiler and build setting"));
}

ToolChainHelpDialog::~ToolChainHelpDialog()
{
    delete m_ui;
}

/*************************************
 * ProjectBuildSettingPage
 *************************************/

ProjectBuildSettingPage::ProjectBuildSettingPage(QWidget *parent) :
    QWizardPage(parent)
{
    d = new ProjectBuildSettingPageData;
    d->ui = new Ui::CompilerConfigurationPage;
    d->ui->setupUi(this);

    d->ui->pushButtonAccept->hide();
    d->ui->pushButtonCancel->hide();
//    d->ui->pushButtonHelp->setIcon(qApp->style()->standardIcon(QStyle::SP_DialogHelpButton));
//    connect(d->ui->pushButtonHelp, SIGNAL(clicked()),
//            this, SLOT(showHelpDialog()));

    //add all toolkits in combobox
    Q_FOREACH(ProjectExplorer::Kit* kit, ProjectExplorer::KitManager::instance()->kits())
        d->ui->comboBoxToolKits->addItem(kit->icon(), kit->displayName(), kit->id().toString());

    this->setMinimumSize(600, 300);
}

ProjectBuildSettingPage::~ProjectBuildSettingPage()
{
    delete d;
    d = 0;
}

BuildSettingParameters ProjectBuildSettingPage::buildParameters() const
{
    BuildSettingParameters param;
    param.cpu = d->ui->lineEditArmCpu->text();
    param.cflags = d->ui->lineEditCFlags->text();
    param.asFlags = d->ui->lineEditAsselblerFlags->text();
    param.makeBinFile = d->ui->checkBoxBin->isChecked();
    param.makeHexFile = d->ui->checkBoxHex->isChecked();
    param.kitId = d->ui->comboBoxToolKits->itemData(d->ui->comboBoxToolKits->currentIndex()).toString();
    return param;
}

void ProjectBuildSettingPage::setBuildParameters(const BuildSettingParameters &parameters)
{
    d->ui->lineEditArmCpu->setText(parameters.cpu);
    d->ui->lineEditCFlags->setText(parameters.cflags);
    d->ui->lineEditAsselblerFlags->setText(parameters.asFlags);
    d->ui->checkBoxBin->setChecked(parameters.makeBinFile);
    d->ui->checkBoxHex->setChecked(parameters.makeHexFile);
}

void ProjectBuildSettingPage::showHelpDialog()
{
    ToolChainHelpDialog helpDialog(this);
    helpDialog.exec();
}

//Qdialog for buildSetting
KitSelectionDialog::KitSelectionDialog(QWidget *parent)
    :QDialog(parent)
{
    d = new ProjectBuildSettingPageData;
    d->ui = new Ui::CompilerConfigurationPage;
    d->ui->setupUi(this);
    this->setWindowTitle(tr("Configure project Build Setting"));
//    d->ui->pushButtonHelp->setIcon(qApp->style()->standardIcon(QStyle::SP_DialogHelpButton));
//    connect(d->ui->pushButtonHelp, SIGNAL(clicked()),
//            this, SLOT(showHelpDialog()));

    //add all toolkits in combobox
    Q_FOREACH(ProjectExplorer::Kit* kit, ProjectExplorer::KitManager::instance()->kits())
        d->ui->comboBoxToolKits->addItem(kit->icon(), kit->displayName(), kit->id().toString());

    connect(d->ui->pushButtonAccept, SIGNAL(clicked()), this, SLOT(accept()));
    connect(d->ui->pushButtonCancel, SIGNAL(clicked()), this, SLOT(reject()));

}

KitSelectionDialog::~KitSelectionDialog()
{
    delete d;
    d = 0;
}

BuildSettingParameters KitSelectionDialog::buildParameters() const
{
    BuildSettingParameters param;
    param.cpu = d->ui->lineEditArmCpu->text();
    param.cflags = d->ui->lineEditCFlags->text();
    param.asFlags = d->ui->lineEditAsselblerFlags->text();
    param.makeBinFile = d->ui->checkBoxBin->isChecked();
    param.makeHexFile = d->ui->checkBoxHex->isChecked();
    param.kitId = d->ui->comboBoxToolKits->itemData(d->ui->comboBoxToolKits->currentIndex()).toString();
    return param;
}

void KitSelectionDialog::setBuildParameters(const BuildSettingParameters &parameters)
{
    d->ui->lineEditArmCpu->setText(parameters.cpu);
    d->ui->lineEditCFlags->setText(parameters.cflags);
    d->ui->lineEditAsselblerFlags->setText(parameters.asFlags);
    d->ui->checkBoxBin->setChecked(parameters.makeBinFile);
    d->ui->checkBoxHex->setChecked(parameters.makeHexFile);
}

void KitSelectionDialog::showHelpDialog()
{
    ToolChainHelpDialog helpDialog(this);
    helpDialog.exec();
}

/*************************************
 * BareEmbeddedProjectWizardDialog
 *************************************/

BareEmbeddedProjectWizardDialog::BareEmbeddedProjectWizardDialog(QWidget *parent) :
    Utils::Wizard(parent)
{
    d = new BareEmbeddedProjectWizardData;

    setWindowTitle(tr("Create a bare minimal arm project"));
    this->setMinimumWidth(700);

    //first page select project name and folder
    d->projectNameLocationPage = new Utils::FileWizardPage(this);
    d->projectNameLocationPage->setTitle(tr("Project Name and Location"));
    d->projectNameLocationPage->setFileNameLabel(tr("Project name:"));
    d->projectNameLocationPage->setPathLabel(tr("Location:"));

    const int projectPageId = addPage(d->projectNameLocationPage);
    wizardProgress()->item(projectPageId)->setTitle(tr("Location"));

    //2nd page project build and compiler setting
    d->projectBuildSetting = new ProjectBuildSettingPage(this);
    d->projectBuildSetting->setTitle(tr("Project setting"));

    const int buildPageId = addPage(d->projectBuildSetting);
    wizardProgress()->item(buildPageId)->setTitle(tr("Build Setting"));

    //3rd page project linker setting
    d->linkerSettingPage = new LinkerSettingPage(this);
    d->linkerSettingPage->setTitle(tr("Linker setting"));
    const int linkerPageId = addPage(d->linkerSettingPage);
    wizardProgress()->item(linkerPageId)->setTitle(tr("Linker Setting"));

    //4th PAge for StartUp file Setting
    d->startUpSettingPage = new StartUpFileSettingPage(this);
    d->startUpSettingPage->setTitle(tr("StartUp Assembly File"));
    const int lstartUpPageId = addPage(d->startUpSettingPage);
    wizardProgress()->item(lstartUpPageId)->setTitle(tr("StartUp setting"));

}

BareEmbeddedProjectWizardDialog::~BareEmbeddedProjectWizardDialog()
{
    delete d;
    d = 0;
}

QString BareEmbeddedProjectWizardDialog::path() const
{
    return d->projectNameLocationPage->path();
}

void BareEmbeddedProjectWizardDialog::setPath(const QString &path)
{
    d->projectNameLocationPage->setPath(path);
}

QString BareEmbeddedProjectWizardDialog::projectName() const
{
    return d->projectNameLocationPage->fileName();
}

BuildSettingParameters BareEmbeddedProjectWizardDialog::buildParameters() const
{
    return d->projectBuildSetting->buildParameters();
}

QString BareEmbeddedProjectWizardDialog::linkerString() const
{
    return d->linkerSettingPage->linkerString();
}

QString BareEmbeddedProjectWizardDialog::startUpString() const
{
    return d->startUpSettingPage->startUpString();
}

/*******************************************************
 * Project Wizard Class
 ********************************************************/

BareEmbeddedProjectWizard::BareEmbeddedProjectWizard() :
    Core::BaseFileWizard(parameters())
{
}

Core::BaseFileWizardParameters BareEmbeddedProjectWizard::parameters()
{
    Core::BaseFileWizardParameters parameters(ProjectWizard);
    {
        const QLatin1String iconPath((":/images/project_icon.png"));
        QPixmap icon(iconPath);
        parameters.setIcon(icon);
    }

    parameters.setDisplayName(tr("Bare minimal arm project"));
    parameters.setId(QLatin1String("Z.Makefile"));
    parameters.setDescription(tr("Create a bare minimal ARM project. It will create a generic startup and linker files"
                                 " using project wizard."));
    parameters.setCategory(QLatin1String(ProjectExplorer::Constants::PROJECT_WIZARD_CATEGORY));
    parameters.setDisplayCategory(QLatin1String(ProjectExplorer::Constants::PROJECT_WIZARD_CATEGORY_DISPLAY));
    parameters.setFlags(Core::IWizard::PlatformIndependent);
    return parameters;
}

QWizard *BareEmbeddedProjectWizard::createWizardDialog(QWidget *parent, const Core::WizardDialogParameters &wizardDialogParameters) const
{
    BareEmbeddedProjectWizardDialog *wizard = new BareEmbeddedProjectWizardDialog(parent);
    setupWizard(wizard);

    wizard->setPath(wizardDialogParameters.defaultPath());

    foreach (QWizardPage *p, wizardDialogParameters.extensionPages())
        BaseFileWizard::applyExtensionPageShortTitle(wizard, wizard->addPage(p));

    return wizard;
}

Core::GeneratedFiles BareEmbeddedProjectWizard::generateFiles(const QWizard *w, QString *errorMessage) const
{
    Q_UNUSED(errorMessage)

    Core::GeneratedFiles files;
    const BareEmbeddedProjectWizardDialog *wizard = qobject_cast<const BareEmbeddedProjectWizardDialog *>(w);
    const QString projectName = wizard->projectName();
    const QString projectPath = wizard->path() + QLatin1String("/") +projectName;
    const QDir dir(projectPath);
    const QString projectFileName = QFileInfo(dir, projectName + QLatin1String(".chops")).absoluteFilePath();
    const QString mainFileName = QFileInfo(dir, QLatin1String("main.c")).absoluteFilePath();
    const QString linkerFileName = QFileInfo(dir, QLatin1String("ROM.ld")).absoluteFilePath();
    const QString startUpFileName = QFileInfo(dir, QLatin1String("Start.S")).absoluteFilePath();
    const QString asmHeaderFileName = QFileInfo(dir, QLatin1String("Asm.h")).absoluteFilePath();

    QStringList allFiles;
    //done append project file it should not be editable
    allFiles.append(mainFileName);
    allFiles.append(linkerFileName);
    allFiles.append(startUpFileName);
    allFiles.append(asmHeaderFileName);

    Core::GeneratedFile projectFile(projectFileName);
    projectFile.setContents(QLatin1String(createProjectFileContent(
                                              wizard->buildParameters(), allFiles)));

    projectFile.setAttributes(Core::GeneratedFile::OpenProjectAttribute);
    files.append(projectFile);

    //main file
    Core::GeneratedFile mainFile(mainFileName);
    mainFile.setContents(QLatin1String("#include \"Asm.h\"\n"
                                       "int main()\n"
                                       "{\n"
                                       "    return 0;\n"
                                       "}\n"));
    files.append(mainFile);

    //linker file
    Core::GeneratedFile linkerFile(linkerFileName);
    linkerFile.setContents(wizard->linkerString());
    files.append(linkerFile);

    //startupFile
    Core::GeneratedFile startUpFile(startUpFileName);
    startUpFile.setContents(wizard->startUpString());
    files.append(startUpFile);

    //Asm File
    Core::GeneratedFile asmHeaderFile(asmHeaderFileName);
    QString asmHeaderString;
    QFile asmFile(QLatin1String(":/Template/AsmHeaderString"));
    if(asmFile.open(QIODevice::ReadOnly))
        asmHeaderString = QLatin1String(asmFile.readAll());
    asmHeaderFile.setContents(asmHeaderString);

    files.append(asmHeaderFile);
    return files;
}

bool BareEmbeddedProjectWizard::postGenerateFiles(const QWizard *w, const Core::GeneratedFiles &l, QString *errorMessage)
{
    Q_UNUSED(w);
    return ProjectExplorer::CustomProjectWizard::postGenerateOpen(l, errorMessage);
}

/*************************************
 * ProjectLinkerSettingPage
 *************************************/


LinkerSettingPage::LinkerSettingPage(QWidget *parent) :
    QWizardPage(parent)
{
    m_ui = new Ui::LinkerConfigPage;
    m_ui->setupUi(this);
}

LinkerSettingPage::~LinkerSettingPage()
{
    delete m_ui;
}

LinkerParameter LinkerSettingPage::linkerParameters()
{
    LinkerParameter param;
    param.romStartAddress = m_ui->leRomOrigin->text();
    param.ramStartAddress = m_ui->leRamOrigin->text();
    param.romsize = m_ui->leRomSize->text();
    param.ramSize = m_ui->leRamSize->text();
    return param;
}

void LinkerSettingPage::setLinkerParameters(const LinkerParameter &parameters)
{
    m_ui->leRomOrigin->setText(parameters.romStartAddress);
    m_ui->leRamOrigin->setText(parameters.ramStartAddress);
    m_ui->leRomSize->setText(parameters.romsize);
    m_ui->leRamSize->setText(parameters.ramSize);
}

QString LinkerSettingPage::linkerString() const
{
    QString linkerString;
    QFile linkerFile(QLatin1String(":/Template/linkerString"));
    if(linkerFile.open(QIODevice::ReadOnly))
        linkerString = QLatin1String(linkerFile.readAll());

    linkerString.replace(QLatin1String("%1"), m_ui->leRomOrigin->text());
    linkerString.replace(QLatin1String("%2"), m_ui->leRomSize->text());
    linkerString.replace(QLatin1String("%3"), m_ui->leRamOrigin->text());
    linkerString.replace(QLatin1String("%4"), m_ui->leRamSize->text());

    return linkerString;
}

/*************************************
 * StartUp File SettingPage
 *************************************/

StartUpFileSettingPage::StartUpFileSettingPage(QWidget *parent) :
    QWizardPage(parent)
{
    m_ui = new Ui::StartUpFilePage;
    m_ui->setupUi(this);
}

StartUpFileSettingPage::~StartUpFileSettingPage()
{
    delete m_ui;
}

void StartUpFileSettingPage::setStartUpFileParameters(const startUpFileParameter &parameters)
{
    m_ui->leUndSize->setText(parameters.undStack);
    m_ui->leAbrtSize->setText(parameters.abrtStack);
    m_ui->leFiqSize->setText(parameters.fiqStack);
    m_ui->leIrqSize->setText(parameters.irqStack);
    m_ui->leSvcSize->setText(parameters.svcStack);
    m_ui->leSysSize->setText(parameters.sysStack);
}

QString StartUpFileSettingPage::startUpString() const
{
    QString startUpString;
    QFile startUpFile(QLatin1String(":/Template/StartUpFileString"));
    if(startUpFile.open(QIODevice::ReadOnly))
        startUpString = QLatin1String(startUpFile.readAll());

    startUpString.replace(QLatin1String("%1"), m_ui->leUndSize->text());
    startUpString.replace(QLatin1String("%2"), m_ui->leIrqSize->text());
    startUpString.replace(QLatin1String("%3"), m_ui->leFiqSize->text());
    startUpString.replace(QLatin1String("%4"), m_ui->leSvcSize->text());
    startUpString.replace(QLatin1String("%5"), m_ui->leAbrtSize->text());
    startUpString.replace(QLatin1String("%6"), m_ui->leSysSize->text());

    return startUpString;
}


} //Internal
} //BareEmbeddedProjectManager
