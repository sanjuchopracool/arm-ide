#include "embeddeddevicefactory.h"
#include "bareembeddedprojectmanagerconstants.h"
#include "embeddeddevice.h"
namespace BareEmbeddedProjectManager {
namespace Internal {

EmbeddedDeviceFactory::EmbeddedDeviceFactory()
{
    setObjectName(QLatin1String("EmbeddedDeviceFactory"));
}

QString EmbeddedDeviceFactory::displayNameForId(Core::Id type) const
{
    if (type == Constants::EMBEDDED_DEVICE_TYPE)
        return tr("Embedded Device");
    return QString();
}

QList<Core::Id> EmbeddedDeviceFactory::availableCreationIds() const
{
    return QList<Core::Id>() << Core::Id(Constants::EMBEDDED_DEVICE_TYPE);
}

bool EmbeddedDeviceFactory::canCreate() const
{
    return false;
}

IDevice::Ptr EmbeddedDeviceFactory::create(Core::Id id) const
{
    Q_UNUSED(id)
    return ProjectExplorer::IDevice::Ptr();
}

bool EmbeddedDeviceFactory::canRestore(const QVariantMap &map) const
{
    return ProjectExplorer::IDevice::typeFromMap(map) == Constants::EMBEDDED_DEVICE_TYPE;
}

IDevice::Ptr EmbeddedDeviceFactory::restore(const QVariantMap &map) const
{
    Q_UNUSED(map)
    return ProjectExplorer::IDevice::Ptr(new EmbeddedDevice);
}

} // Internal
} //BareEmbeddedProjectManager
