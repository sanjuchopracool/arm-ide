#include "chopsprojectmanager.h"
#include "bareembeddedprojectmanagerconstants.h"
#include "bareembeddedproject.h"
#include <QFileInfo>

namespace BareEmbeddedProjectManager {
namespace Internal {

ChopsProjectManager::ChopsProjectManager()
{
}

QString ChopsProjectManager::mimeType() const
{
    return QString(QLatin1String(Constants::CHOPSPROJECTMIMETYPE));
}

ProjectExplorer::Project *ChopsProjectManager::openProject(
        const QString &fileName, QString *errorString)
{
    if (!QFileInfo(fileName).isFile()) {
        if (errorString)
            *errorString = tr("Failed opening project '%1': Project is not a file")
                .arg(fileName);
        return 0;
    }

    return new BareEmbeddedProject(this, fileName);
}

void ChopsProjectManager::registerProject(BareEmbeddedProject *project)
{
    m_projects.append(project);
}

void ChopsProjectManager::unregisterProject(BareEmbeddedProject *project)
{
    m_projects.removeAll(project);
}

} // Internal
} //BareEmbeddedProjectManager
