#include "embeddeddevice.h"
#include "bareembeddedprojectmanagerconstants.h"
#include <QCoreApplication>

namespace BareEmbeddedProjectManager {
namespace Internal {

EmbeddedDevice::EmbeddedDevice() :
    IDevice(Constants::EMBEDDED_DEVICE_TYPE,
            IDevice::AutoDetected,
            IDevice::Hardware,
            Core::Id(Constants::EMBEDDED_DEVICE_ID))
{
    setDisplayName(QCoreApplication::translate("Embedded::Internal::EmbeddedDevice", "Run on Embedded Device"));
    setDeviceState(DeviceReadyToUse);
}

EmbeddedDevice::EmbeddedDevice(const EmbeddedDevice &other)
    : IDevice(other)
{ }

QString EmbeddedDevice::displayType() const
{
    return QCoreApplication::translate("Embedded::Internal::EmbeddedDevice", "Run on Embedded Device");
}

IDeviceWidget *EmbeddedDevice::createWidget()
{
    return 0;
}

QList<Core::Id> EmbeddedDevice::actionIds() const
{
    return QList<Core::Id>();
}

QString EmbeddedDevice::displayNameForActionId(Core::Id actionId) const
{
    Q_UNUSED(actionId)
    return QString();
}

void EmbeddedDevice::executeAction(Core::Id actionId, QWidget *parent) const
{
    Q_UNUSED(actionId)
    Q_UNUSED(parent)
}

IDevice::Ptr EmbeddedDevice::clone() const
{
    return IDevice::Ptr(new EmbeddedDevice(*this));
}

} // Internal
} //BareEmbeddedProjectManager
