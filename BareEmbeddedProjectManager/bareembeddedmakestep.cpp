#include "bareembeddedmakestep.h"
#include "bareembeddedprojectmanagerconstants.h"
#include "bareembeddedproject.h"
#include "bareembeddedbuildconfiguration.h"

#include <extensionsystem/pluginmanager.h>
#include <projectexplorer/buildsteplist.h>
#include <projectexplorer/gnumakeparser.h>
#include <projectexplorer/kitinformation.h>
#include <projectexplorer/projectexplorer.h>
#include <projectexplorer/projectexplorerconstants.h>
#include <projectexplorer/toolchain.h>
#include <qtsupport/qtkitinformation.h>
#include <qtsupport/qtparser.h>
#include <coreplugin/variablemanager.h>
#include <utils/stringutils.h>
#include <utils/qtcassert.h>
#include <utils/qtcprocess.h>
#include "bareembeddedproject.h"

using namespace Core;
using namespace ProjectExplorer;

namespace BareEmbeddedProjectManager {
namespace Internal {

const char BAREENBEDDED_MS_ID[] = "ChopsProjectManager.BareEmbeddedMakeStep";
const char BAREEMBEDDED_MS_DISPLAY_NAME[] = QT_TRANSLATE_NOOP("ChopsProjectManager::Internal::BareEmbeddedMakeStep",
                                                     "Make");

const char BUILD_TARGETS_KEY[] = "ChopsProjectManager.BareEmbeddedMakeStep.BuildTargets";
const char MAKE_ARGUMENTS_KEY[] = "ChopsProjectManager.BareEmbeddedMakeStep.MakeArguments";
const char MAKE_COMMAND_KEY[] = "ChopsProjectManager.BareEmbeddedMakeStep.MakeCommand";
const char CLEAN_KEY[] = "ChopsProjectManager.BareEmbeddedMakeStep.Clean";

BareEmbeddedMakeStep::BareEmbeddedMakeStep(BuildStepList *parent) :
    AbstractProcessStep(parent, Id(BAREENBEDDED_MS_ID)),
    m_clean(false)
{
    ctor();
}

BareEmbeddedMakeStep::BareEmbeddedMakeStep(BuildStepList *parent, const Id id) :
    AbstractProcessStep(parent, id),
    m_clean(false)
{
    ctor();
}

BareEmbeddedMakeStep::BareEmbeddedMakeStep(BuildStepList *parent, BareEmbeddedMakeStep *bs) :
    AbstractProcessStep(parent, bs),
    m_buildTargets(bs->m_buildTargets),
    m_makeArguments(bs->m_makeArguments),
    m_makeCommand(bs->m_makeCommand),
    m_clean(bs->m_clean)
{
    ctor();
}

void BareEmbeddedMakeStep::ctor()
{
    setDefaultDisplayName(QCoreApplication::translate("ChopsProjectManager::Internal::BareEmbeddedMakeStep",
                                                      BAREEMBEDDED_MS_DISPLAY_NAME));
}

BareEmbeddedMakeStep::~BareEmbeddedMakeStep()
{
}

bool BareEmbeddedMakeStep::init()
{
    BuildConfiguration *bc = buildConfiguration();
    if (!bc)
        bc = target()->activeBuildConfiguration();

    m_tasks.clear();
    ToolChain *tc = ToolChainKitInformation::toolChain(target()->kit());
    if (!tc) {
        m_tasks.append(Task(Task::Error, tr("Qt Creator needs a compiler set up to build. Configure a compiler in the kit options."),
                            Utils::FileName(), -1,
                            Core::Id(ProjectExplorer::Constants::TASK_CATEGORY_BUILDSYSTEM)));
        return true; // otherwise the tasks will not get reported
    }

    ProcessParameters *pp = processParameters();
    pp->setMacroExpander(bc->macroExpander());
    pp->setWorkingDirectory(bc->buildDirectory());
    Utils::Environment env = bc->environment();
    // Force output to english for the parsers. Do this here and not in the toolchain's
    // addToEnvironment() to not screw up the users run environment.
    env.set(QLatin1String("LC_ALL"), QLatin1String("C"));
    pp->setEnvironment(env);
    pp->setCommand(makeCommand(bc->environment()));
    pp->setArguments(allArguments());
    pp->resolveAll();

    // If we are cleaning, then make can fail with an error code, but that doesn't mean
    // we should stop the clean queue
    // That is mostly so that rebuild works on an already clean project
    setIgnoreReturnValue(m_clean);

    setOutputParser(new GnuMakeParser());
    IOutputParser *parser = target()->kit()->createOutputParser();
    if (parser)
        appendOutputParser(parser);
    outputParser()->setWorkingDirectory(pp->effectiveWorkingDirectory());

    return AbstractProcessStep::init();
}

void BareEmbeddedMakeStep::setClean(bool clean)
{
    m_clean = clean;
}

bool BareEmbeddedMakeStep::isClean() const
{
    return m_clean;
}

QVariantMap BareEmbeddedMakeStep::toMap() const
{
    QVariantMap map(AbstractProcessStep::toMap());

    map.insert(QLatin1String(BUILD_TARGETS_KEY), m_buildTargets);
    map.insert(QLatin1String(MAKE_ARGUMENTS_KEY), m_makeArguments);
    map.insert(QLatin1String(MAKE_COMMAND_KEY), m_makeCommand);
    map.insert(QLatin1String(CLEAN_KEY), m_clean);
    return map;
}

bool BareEmbeddedMakeStep::fromMap(const QVariantMap &map)
{
    m_buildTargets = map.value(QLatin1String(BUILD_TARGETS_KEY)).toStringList();
    m_makeArguments = map.value(QLatin1String(MAKE_ARGUMENTS_KEY)).toString();
    m_makeCommand = map.value(QLatin1String(MAKE_COMMAND_KEY)).toString();
    m_clean = map.value(QLatin1String(CLEAN_KEY)).toBool();

    return BuildStep::fromMap(map);
}

QString BareEmbeddedMakeStep::allArguments() const
{
    QString args = m_makeArguments;
    Utils::QtcProcess::addArgs(&args, m_buildTargets);
    return args;
}

QString BareEmbeddedMakeStep::makeCommand(const Utils::Environment &environment) const
{
    QString command = m_makeCommand;
    if (command.isEmpty()) {
        ToolChain *tc = ToolChainKitInformation::toolChain(target()->kit());
        if (tc)
            command = tc->makeCommand(environment);
        else
            command = QLatin1String("make");
    }
    return command;
}

void BareEmbeddedMakeStep::run(QFutureInterface<bool> &fi)
{
    //find Project generate makefile
    BareEmbeddedProject* project = qobject_cast<BareEmbeddedProject*>(this->target()->project());
    if(!project)
        return;


    //generate makefile
    if(m_buildTargets.count() && m_buildTargets.first() != QLatin1String("clean"))
    {
        if(!project->generateMakeFile())
            return;
    }

    bool canContinue = true;
    foreach (const Task &t, m_tasks) {
        addTask(t);
        canContinue = false;
    }
    if (!canContinue) {
        emit addOutput(tr("Configuration is faulty. Check the Issues view for details."), BuildStep::MessageOutput);
        fi.reportResult(false);
        emit finished();
        return;
    }

    AbstractProcessStep::run(fi);
}

BuildStepConfigWidget *BareEmbeddedMakeStep::createConfigWidget()
{
    return new BareEmbeddedMakeStepConfigWidget(this);
}

bool BareEmbeddedMakeStep::immutable() const
{
    return false;
}

bool BareEmbeddedMakeStep::buildsTarget(const QString &target) const
{
    return m_buildTargets.contains(target);
}

void BareEmbeddedMakeStep::setBuildTarget(const QString &target, bool on)
{
    QStringList old = m_buildTargets;
    if (on && !old.contains(target))
         old << target;
    else if (!on && old.contains(target))
        old.removeOne(target);

    m_buildTargets = old;
}

//
// BareEmbeddedMakeStepConfigWidget
//

BareEmbeddedMakeStepConfigWidget::BareEmbeddedMakeStepConfigWidget(BareEmbeddedMakeStep *makeStep)
    : m_makeStep(makeStep)
{
    m_ui = new Ui::BareEmbeddedMakeStepWidget;
    m_ui->setupUi(this);

    BareEmbeddedProject *pro = static_cast<BareEmbeddedProject *>(m_makeStep->target()->project());
    foreach (const QString &target, pro->buildTargets()) {
        QListWidgetItem *item = new QListWidgetItem(target, m_ui->targetsList);
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        item->setCheckState(m_makeStep->buildsTarget(item->text()) ? Qt::Checked : Qt::Unchecked);
    }

    m_ui->makeLineEdit->setText(m_makeStep->m_makeCommand);
    m_ui->makeArgumentsLineEdit->setText(m_makeStep->m_makeArguments);
    updateMakeOverrrideLabel();
    updateDetails();

    connect(m_ui->targetsList, SIGNAL(itemChanged(QListWidgetItem*)),
            this, SLOT(itemChanged(QListWidgetItem*)));
    connect(m_ui->makeLineEdit, SIGNAL(textEdited(QString)),
            this, SLOT(makeLineEditTextEdited()));
    connect(m_ui->makeArgumentsLineEdit, SIGNAL(textEdited(QString)),
            this, SLOT(makeArgumentsLineEditTextEdited()));

    connect(ProjectExplorerPlugin::instance(), SIGNAL(settingsChanged()),
            this, SLOT(updateMakeOverrrideLabel()));
    connect(ProjectExplorerPlugin::instance(), SIGNAL(settingsChanged()),
            this, SLOT(updateDetails()));

    connect(m_makeStep->target(), SIGNAL(kitChanged()),
            this, SLOT(updateMakeOverrrideLabel()));

    connect(pro, SIGNAL(environmentChanged()),
            this, SLOT(updateMakeOverrrideLabel()));
    connect(pro, SIGNAL(environmentChanged()),
            this, SLOT(updateDetails()));
}

BareEmbeddedMakeStepConfigWidget::~BareEmbeddedMakeStepConfigWidget()
{
    delete m_ui;
}

QString BareEmbeddedMakeStepConfigWidget::displayName() const
{
    return tr("Make", "BareEmbeddedMakestep display name.");
}

void BareEmbeddedMakeStepConfigWidget::updateMakeOverrrideLabel()
{
    BuildConfiguration *bc = m_makeStep->buildConfiguration();
    if (!bc)
        bc = m_makeStep->target()->activeBuildConfiguration();

    m_ui->makeLabel->setText(tr("Override %1:").arg(m_makeStep->makeCommand(bc->environment())));
}

void BareEmbeddedMakeStepConfigWidget::updateDetails()
{
    BuildConfiguration *bc = m_makeStep->buildConfiguration();
    if (!bc)
        bc = m_makeStep->target()->activeBuildConfiguration();

    ProcessParameters param;
    param.setMacroExpander(bc->macroExpander());
    param.setWorkingDirectory(bc->buildDirectory());
    param.setEnvironment(bc->environment());
    param.setCommand(m_makeStep->makeCommand(bc->environment()));
    param.setArguments(m_makeStep->allArguments());
    m_summaryText = param.summary(displayName());
    emit updateSummary();
}

QString BareEmbeddedMakeStepConfigWidget::summaryText() const
{
    return m_summaryText;
}

void BareEmbeddedMakeStepConfigWidget::itemChanged(QListWidgetItem *item)
{
    m_makeStep->setBuildTarget(item->text(), item->checkState() & Qt::Checked);
    updateDetails();
}

void BareEmbeddedMakeStepConfigWidget::makeLineEditTextEdited()
{
    m_makeStep->m_makeCommand = m_ui->makeLineEdit->text();
    updateDetails();
}

void BareEmbeddedMakeStepConfigWidget::makeArgumentsLineEditTextEdited()
{
    m_makeStep->m_makeArguments = m_ui->makeArgumentsLineEdit->text();
    updateDetails();
}

//
// BareEmbeddedMakeStepFactory
//

BareEmbeddedMakeStepFactory::BareEmbeddedMakeStepFactory(QObject *parent) :
    IBuildStepFactory(parent)
{
}

bool BareEmbeddedMakeStepFactory::canCreate(BuildStepList *parent, const Id id) const
{
    if (parent->target()->project()->id() == Constants::CHOPSPROJECT_ID)
        return id == BAREENBEDDED_MS_ID;
    return false;
}

BuildStep *BareEmbeddedMakeStepFactory::create(BuildStepList *parent, const Id id)
{
    if (!canCreate(parent, id))
        return 0;
    BareEmbeddedMakeStep *step = new BareEmbeddedMakeStep(parent);
    if (parent->id() == ProjectExplorer::Constants::BUILDSTEPS_CLEAN) {
        step->setClean(true);
        step->setBuildTarget(QLatin1String("clean"), /* on = */ true);
    } else if (parent->id() == ProjectExplorer::Constants::BUILDSTEPS_BUILD) {
        step->setBuildTarget(QLatin1String("all"), /* on = */ true);
    }
    return step;
}

bool BareEmbeddedMakeStepFactory::canClone(BuildStepList *parent, BuildStep *source) const
{
    return canCreate(parent, source->id());
}

BuildStep *BareEmbeddedMakeStepFactory::clone(BuildStepList *parent, BuildStep *source)
{
    if (!canClone(parent, source))
        return 0;
    BareEmbeddedMakeStep *old(qobject_cast<BareEmbeddedMakeStep *>(source));
    Q_ASSERT(old);
    return new BareEmbeddedMakeStep(parent, old);
}

bool BareEmbeddedMakeStepFactory::canRestore(BuildStepList *parent, const QVariantMap &map) const
{
    return canCreate(parent, idFromMap(map));
}

BuildStep *BareEmbeddedMakeStepFactory::restore(BuildStepList *parent, const QVariantMap &map)
{
    if (!canRestore(parent, map))
        return 0;
    BareEmbeddedMakeStep *bs(new BareEmbeddedMakeStep(parent));
    if (bs->fromMap(map))
        return bs;
    delete bs;
    return 0;
}

QList<Id> BareEmbeddedMakeStepFactory::availableCreationIds(BuildStepList *parent) const
{
    if (parent->target()->project()->id() == Constants::CHOPSPROJECT_ID)
        return QList<Id>() << Id(BAREENBEDDED_MS_ID);
    return QList<Id>();
}

QString BareEmbeddedMakeStepFactory::displayNameForId(const Id id) const
{
    if (id == BAREENBEDDED_MS_ID)
        return QCoreApplication::translate("ChopsProjectManager::Internal::BareEmbeddedMakeStep",
                                           BAREEMBEDDED_MS_DISPLAY_NAME);
    return QString();
}

} // namespace Internal
} // namespace BareEmbeddedProjectManager
