#ifndef BAREEMBEDDEDPROJECTNODE_H
#define BAREEMBEDDEDPROJECTNODE_H
#include <projectexplorer/projectnodes.h>
#include <coreplugin/idocument.h>

using namespace ProjectExplorer;
namespace BareEmbeddedProjectManager {
namespace Internal {

class BareEmbeddedProject;
struct BareEmbeddedProjectNodeData;

class BareEmbeddedProjectNode : public ProjectExplorer::ProjectNode
{
public:
    BareEmbeddedProjectNode(BareEmbeddedProject *project, Core::IDocument *projectFile);
    ~BareEmbeddedProjectNode();

    Core::IDocument *projectFile() const;
    QString projectFilePath() const;

    bool hasBuildTargets() const;

    QList<ProjectAction> supportedActions(Node *node) const;

    bool canAddSubProject(const QString &proFilePath) const;

    bool addSubProjects(const QStringList &proFilePaths);

    bool removeSubProjects(const QStringList &proFilePaths);

    bool addFiles(const FileType fileType,
                  const QStringList &filePaths,
                  QStringList *notAdded);
    // TODO: Maybe remove fileType, can be detected by project
    bool removeFiles(const FileType fileType,
                     const QStringList &filePaths,
                     QStringList *notRemoved);
    bool deleteFiles(const FileType fileType,
                     const QStringList &filePaths);
    bool renameFile(const FileType fileType,
                    const QString &filePath,
                    const QString &newFilePath);

    // TODO node parameter not really needed
    QList<ProjectExplorer::RunConfiguration *> runConfigurationsFor(Node *node);

private:
    BareEmbeddedProjectNodeData* d;
};

} // Internal
} //BareEmbeddedProjectManager

#endif // BAREEMBEDDEDPROJECTNODE_H
