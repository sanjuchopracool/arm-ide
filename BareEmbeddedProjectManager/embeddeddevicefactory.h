#ifndef EMBEDDEDDEVICEFACTORY_H
#define EMBEDDEDDEVICEFACTORY_H

#include <projectexplorer/devicesupport/idevicefactory.h>

using namespace ProjectExplorer;
namespace BareEmbeddedProjectManager {
namespace Internal {

class EmbeddedDeviceFactory : public ProjectExplorer::IDeviceFactory
{
    Q_OBJECT

public:
    EmbeddedDeviceFactory();

    QString displayNameForId(Core::Id type) const;
    QList<Core::Id> availableCreationIds() const;
    bool canCreate() const;
    IDevice::Ptr create(Core::Id id) const;
    bool canRestore(const QVariantMap &map) const;
    IDevice::Ptr restore(const QVariantMap &map) const;
};

} // Internal
} //BareEmbeddedProjectManager

#endif // EMBEDDEDDEVICEFACTORY_H
